<?php namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

use Input;

class Model extends Eloquent {

    public function store(array $data = array(), array $merge = array())
    {
        if (empty($data))
            $data = Input::all();
            $data = array_merge($data, $merge);

        if (array_key_exists('_method', $data))
            unset($data['_method']);

        if (array_key_exists('_token', $data))
            unset($data['_token']);
        
        foreach ($data as $attribute => $value)
            if (is_array($value))
                continue;
            else
                $this->attributes[$attribute] = $value;                        
        
        $this->save();

        return $this;
    }

}
