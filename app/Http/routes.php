<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/',         ['as' => 'home',            'uses' => 'HomeController@index']);
Route::get('login',     ['as' => 'login',           'uses' => 'LoginController@index']);
Route::post('login',    ['as' => 'login.submit',    'uses' => 'LoginController@submit']);
Route::get('logout',    ['as' => 'logout',          'uses' => 'LoginController@logout']);

Route::get('scan.php', 'DownloadController@getScan');
Route::controller('register', 'RegisterController');
Route::controller('panduan', 'PanduanController');
Route::controller('instruksi-kerja', 'InstruksiKerjaController');

Route::group(['namespace' => 'Backend', 'prefix' => 'backend', 'middleware' => ['auth', 'admin']], function()
{
    Route::get('/',                                         ['as' => 'backend',                             'uses' => 'HomeController@index']);
    Route::get('permintaan/active',                         ['as' => 'backend.permintaan.active',           'uses' => 'PermintaanController@active']);
    Route::get('permintaan/suspend',                        ['as' => 'backend.permintaan.showsuspend',      'uses' => 'PermintaanController@showSuspend']);
    Route::get('permintaan/{id}/activation',                ['as' => 'backend.permintaan.proccess',         'uses' => 'PermintaanController@proccess']);
    Route::get('permintaan/{id}/sendmail-activation',       ['as' => 'backend.permintaan.active.sendmail',  'uses' => 'PermintaanController@sendMailActivation']);
    Route::get('permintaan/{id}/destroy',                   ['as' => 'backend.permintaan.destroy.confirm',  'uses' => 'PermintaanController@destroyConfirm']);
    Route::get('permintaan/{id}/suspend',                   ['as' => 'backend.permintaan.suspend',          'uses' => 'PermintaanController@suspend']);
    Route::get('permintaan/{id}/unsuspend',                 ['as' => 'backend.permintaan.unsuspend',        'uses' => 'PermintaanController@unsuspend']);

    Route::resource('permintaan',                           'PermintaanController');

    Route::get('user/password/{id}',                        ['as' => 'backend.user.password',               'uses' => 'UserController@password']);
    Route::post('user/password/{id}',                       ['as' => 'backend.user.password.submit',        'uses' => 'UserController@change']);
    Route::resource('user', 'UserController');

    Route::get('upload',                                    ['as' => 'backend.upload.form',                 'uses' => 'UploadController@form']);
    Route::post('upload',                                   ['as' => 'backend.upload.submit',               'uses' => 'UploadController@submit']);

    Route::controller('laporan', 'LaporanController', [
        'getIndex'      => 'backend.laporan.index',
        'postDownload'  => 'backend.laporan.download',
    ]);

});

Route::group(['middleware' => 'auth.mahasiswa', 'namespace' => 'Mahasiswa'], function()
{
    Route::get('download/{dir?}',       ['as' => 'jurnal.show',     'uses' => 'JurnalController@show'])->where('dir', '([A-Za-z]+/?)+');
    Route::get('download-get/{dir?}',   ['as' => 'jurnal.download', 'uses' => 'JurnalController@download']);
});

Route::group(['prefix' => 'mahasiswa', 'namespace' => 'Mahasiswa'], function()
{
    Route::get('login',     ['as' => 'mahasiswa.login',           'uses' => 'LoginController@index']);
    Route::post('login',    ['as' => 'mahasiswa.login.submit',    'uses' => 'LoginController@submit']);

    Route::group(['middleware' => 'auth.mahasiswa'], function()
    {
        Route::get('logout',                ['as' => 'mahasiswa.logout',    'uses' => 'LoginController@logout']);
        Route::get('profile',               ['as' => 'mahasiswa.edit',      'uses' => 'UserController@edit']);
        Route::put('profile',               ['as' => 'mahasiswa.update',    'uses' => 'UserController@update']);
        Route::get('profile/password',      ['as' => 'mahasiswa.password',  'uses' => 'UserController@password']);
        Route::post('profile/password',     ['as' => 'mahasiswa.change',    'uses' => 'UserController@change']);
        Route::get('doc/{nim}',             ['as' => 'mahasiswa.doc',       'uses' => 'DocumentController@show']);
        Route::get('doc/{nim}/{filename}',  ['as' => 'mahasiswa.doc.pdf',   'uses' => 'DocumentController@pdf']);
    });
});


// Validator Extends

Validator::extend('kapasitas', function($attribute, $value, $parameters)
{
    return $value <= 50;
});

Validator::extend('old', function($attribute, $value, $parameters)
{
    $id     = $parameters[0];
    $user   = App\User::find($id);

    return Hash::check($value, $user->password);
});


Route::get('tesmail', function()
{
    $data['mahasiswa'] = App\Mahasiswa::first();

    Mail::send('emails.basic', $data, function($message)
    {
        $message->to('nurmanhabib@gmail.com', 'Habib Nurrahman')->subject('Coba Kirim Email');
    });
});

Route::get('quota', function()
{
    // $quota = new Nurmanhabib\QuotaTool\QuotaTool('/');
    // $quota->uid('habib');
    // $quota->limit(100024, 2000000);
    QuotaTool::filesystem('/');
    QuotaTool::uid('nurmanhabib')->limit('50G', '51G');

    echo QuotaTool::raw();

    echo '<hr>';

    echo QuotaTool::getLimit();
});
