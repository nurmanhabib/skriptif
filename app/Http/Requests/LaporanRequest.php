<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class LaporanRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'angkatan'		=> 'regex:/^\d{4}$/'
		];
	}

	public function messages()
	{
		return [
			'angkatan.regex'	=> 'Format angkatan adalah "YYYY". Contoh: 2011, 2009, 2007'
		];
	}

}
