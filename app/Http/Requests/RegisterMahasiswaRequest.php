<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterMahasiswaRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'nim'			=> 'required|unique:mahasiswa|between:6,10',
			'password'		=> 'required|min:6|confirmed',
			'nama'			=> 'required|between:3,50',
			'email'			=> 'required|email|unique:mahasiswa|unique:users',
			'no_hp'			=> 'required|numeric',
		];
	}

}
