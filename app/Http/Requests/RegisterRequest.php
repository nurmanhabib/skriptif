<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'nim'			=> 'required|unique:mahasiswa|unique:users,name|between:8,10',
			'nama'			=> 'required|between:3,50',
			'email'			=> 'required|unique:mahasiswa|unique:users,email|email',
			'no_hp'			=> 'required|numeric',
			
			'skripsi.judul'				=> 'required|unique:skripsi,judul',
			'skripsi.dosen_pembimbing'	=> 'required',
			'permintaan.kapasitas'		=> 'required|numeric|kapasitas',
		];
	}

	public function messages()
	{
	    return [
	        'nim.unique' 						=> 'NIM sudah terdaftar. Untuk mendaftar hosting, silahkan login dengan NIM Anda terlebih dahulu.',
	        'skripsi.judul.required' 			=> 'The judul skripsi field is required.',
	        'skripsi.judul.unique' 				=> 'Judul skripsi sudah terdaftar.',
	        'skripsi.dosen_pembimbing.required' => 'The dosen pembimbing field is required.',
	        'permintaan.kapasitas.required' 	=> 'The ukuran file field is required.',
	        'permintaan.kapasitas.kapasitas' 	=> 'Kapasitas melampaui batas.',
	    ];
	}

}
