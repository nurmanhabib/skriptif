<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Contracts\Userable;

class ChangePasswordRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'password'		=> 'required|min:3|confirmed',
			'password_old'	=> 'required|old:' . $this->input('id'),
		];
	}

	public function messages()
	{
		return [
			'password_old.old'	=> 'Password lama salah',
		];
	}

}
