<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterExtendRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'skripsi.judul'				=> 'required',
			'skripsi.dosen_pembimbing'	=> 'required',
			'permintaan.kapasitas'		=> 'required|numeric|kapasitas',
		];
	}

	public function messages()
	{
	    return [
	        'skripsi.judul.required' 			=> 'The judul skripsi field is required.',
	        'skripsi.dosen_pembimbing.required' => 'The dosen pembimbing field is required.',
	        'permintaan.kapasitas.required' 	=> 'The ukuran file field is required.',
	        'permintaan.kapasitas.kapasitas' 	=> 'Kapasitas melampaui batas.',
	    ];
	}

}
