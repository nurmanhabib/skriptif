<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

use App\Permintaan;

class PermintaanRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$permintaan	= Permintaan::find($this->input('id'));
		$mahasiswa 	= $permintaan->mahasiswa;
		$skripsi	= $mahasiswa->skripsi;

		return [
			'id'				=> 'required|numeric|exists:permintaan',
			'ftp.password'		=> 'required|min:3|max:8',
			'ftp.quota'			=> 'required|numeric',
			'database.password'	=> 'required|min:3|max:20',
			'skripsi.judul'		=> 'required|min:5|unique:skripsi,judul,' . $skripsi->id,
			'mahasiswa.nim'		=> 'required|between:8,10|unique:mahasiswa,nim,' . $mahasiswa->id,
			'mahasiswa.nama'	=> 'required|min:3',
			'mahasiswa.email'	=> 'required|email',
		];
	}

}
