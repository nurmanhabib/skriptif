<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class InstruksiKerjaController extends Controller {

	public function getIndex()
	{
		return view('page.instruksi-kerja.index')->withPageTitle('Instruksi Kerja');
	}

}
