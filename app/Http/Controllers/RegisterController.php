<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Authenticatable;

use Mail;
use Session;

use App\Http\Requests\RegisterRequest;
use App\Http\Requests\RegisterMahasiswaRequest;
use App\Http\Requests\RegisterExtendRequest;
use App\Mahasiswa;
use App\Skripsi;
use App\Permintaan;

class RegisterController extends Controller {

    public function __construct()
    {
        $this->middleware('new.mahasiswa', ['except' => ['getSuccess']]);
    }

    public function getIndex(Authenticatable $user = null)
    {
        return $this->getHosting($user);
    }

    public function postIndex(RegisterRequest $request)
    {
        return $this->postHosting($request);
    }

    public function getHosting(Authenticatable $user = null)
    {
        if (!empty($user) && !$user->isAdmin())
            return $this->getExtend($user);

        return view('page.register')->with('page_title', 'Form Permintaan Akun Hosting');
    }

    public function postHosting(RegisterRequest $request)
    {
        $mahasiswa = new Mahasiswa;
        $mahasiswa->store();

        $skripsi = new Skripsi;
        $skripsi->mahasiswa_id = $mahasiswa->id;
        $skripsi->store($request->get('skripsi'));

        $permintaan = new Permintaan;
        $permintaan->mahasiswa_id = $mahasiswa->id;
        $permintaan->store($request->get('permintaan'));

        Mail::send('emails.register', ['mahasiswa' => $mahasiswa], function($message) use($mahasiswa)
        {
            $message->to($mahasiswa->email, $mahasiswa->nama)->subject('Registrasi Skripsi TIF UAD');
        });

        return redirect('register/success')->withOk(true);
    }

    public function getSuccess()
    {
        if (Session::has('ok'))
        	return view('page.register_success')->with('page_title', 'Form Permintaan Akun Hosting');
        else
        	return redirect('register');
    }

    public function getExtend(Authenticatable $user)
    {
        view()->share('user', $user);
        view()->share('mahasiswa', $user->profile);

        return view('page.register_extend')->with('page_title', 'Form Permintaan Akun Hosting');
    }

    public function postExtend(Authenticatable $user, RegisterExtendRequest $request)
    {
        $mahasiswa = $user->profile;

        $skripsi = new Skripsi;
        $skripsi->mahasiswa_id = $mahasiswa->id;
        $skripsi->store($request->get('skripsi'));

        $permintaan = new Permintaan;
        $permintaan->mahasiswa_id = $mahasiswa->id;
        $permintaan->store($request->get('permintaan'));

        $mahasiswa = $permintaan->mahasiswa;

        Mail::send('emails.register', ['mahasiswa' => $mahasiswa], function($message) use($mahasiswa)
        {
            $message->to($mahasiswa->email, $mahasiswa->nama)->subject('Registrasi Skripsi TIF UAD');
        });

        return redirect('register/success')->withOk(true);
    }

    public function getJurnal()
    {
        return view('page.register_jurnal')->with('page_title', 'Form Pendaftaran Akun Akses Jurnal');
    }

    public function postJurnal(RegisterMahasiswaRequest $request)
    {
        $mahasiswa = new Mahasiswa;
        $mahasiswa->store($request->except('password_confirmation'));

        Mail::send('emails.register_jurnal', ['mahasiswa' => $mahasiswa], function($message) use($mahasiswa)
        {
            $message->to($mahasiswa->email, $mahasiswa->nama)->subject('Registrasi Akses Jurnal Skripsi TIF UAD');
        });

        return redirect('mahasiswa/login')->withSuccess('Anda sudah berhasil mendaftar.');
    }

}
