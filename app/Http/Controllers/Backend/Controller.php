<?php namespace App\Http\Controllers\Backend;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Navigator;

class Controller extends BaseController {

    public function __construct()
    {
        Navigator::set('Dashboard', route('backend'), 'home');
        Navigator::set('Permintaan', route('backend.permintaan.index'), 'refresh');
        
        Navigator::set('Hosting', '#', 'building')->child(function()
        {
            Navigator::set('Aktif', route('backend.permintaan.active'));
            Navigator::set('Suspend', route('backend.permintaan.showsuspend'));
        });

        Navigator::set('Upload PDF', route('backend.upload.form'), 'file-pdf-o');
        Navigator::set('Users', route('backend.user.index'), 'users');
        Navigator::set('Laporan', route('backend.laporan.index'), 'file-text-o');
        
        Navigator::setTemplate('navigator.sbadmin2');
        Navigator::setActive(route('backend'));
    }

}
