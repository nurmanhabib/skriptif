<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests;

use App\Permintaan;

use Illuminate\Http\Request;

class HomeController extends Controller {

	public function index()
	{
		$pending	= Permintaan::pending()->get();
        $active     = Permintaan::active()->get();
        $suspend    = Permintaan::suspend()->get();

		return view('page.dashboard')
		->with('pending', $pending)
		->with('active', $active)
        ->with('suspend', $suspend);
	}

}
