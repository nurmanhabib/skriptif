<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;

use App\Permintaan;
use App\Skripsi;
use App\Events\PermintaanActivated as Activated;
use Carbon\Carbon;
use Nurmanhabib\Navigator\Facades\Navigator;
use Nurmanhabib\QuotaTool\Facades\QuotaTool;

use CreateFTP;
use CreateDB;
use Mail;

class PermintaanController extends Controller {

    public function __construct()
    {
        parent::__construct();

        Navigator::setActive(route('backend.permintaan.index'));
    }

    public function index(Request $request)
    {
        if ($request->has('cari'))
            return $this->search($request);

        $permintaan = Permintaan::latest()->pending()->paginate(20);

        return view('page.permintaan.index')
            ->with('page_title', 'Permintaan')
            ->with('permintaan', $permintaan);
    }

    public function active()
    {
        Navigator::setActive(route('backend.permintaan.active'));

        $permintaan = Permintaan::active()->latestActivated()->paginate(20);

        return view('page.permintaan.index')
            ->with('page_title', 'Permintaan Aktif')
            ->with('permintaan', $permintaan);
    }

    public function showSuspend()
    {
        Navigator::setActive(route('backend.permintaan.showsuspend'));

        $permintaan = Permintaan::latest()->suspend()->paginate(20);

        return view('page.permintaan.index')
            ->with('page_title', 'Permintaan Suspend')
            ->with('permintaan', $permintaan);
    }

    public function search(Request $request)
    {
        $permintaan = Permintaan::cari($request->get('cari'))->paginate(20);

        return view('page.permintaan.index')
            ->with('page_title', 'Cari Permintaan "' . $request->get('cari') . '"')
            ->with('permintaan', $permintaan);
    }

    public function show($id)
    {
        $permintaan = Permintaan::find($id);
        $mahasiswa  = $permintaan->mahasiswa;
        $skripsi    = $mahasiswa->skripsi;

        return view('page.permintaan.show')
            ->with('page_title', 'Detail FTP and MySQL Account')
            ->with('permintaan', $permintaan)
            ->with('mahasiswa', $mahasiswa)
            ->with('skripsi', $skripsi);
    }

    public function proccess($id)
    {
        $permintaan = Permintaan::find($id);
        $mahasiswa  = $permintaan->mahasiswa;
        $skripsi    = $mahasiswa->skripsi;

        if ($permintaan->status == 'active')
            return $this->show($id);

        return view('page.permintaan.proccess')
            ->with('page_title', 'Aktivasi FTP and MySQL Account')
            ->with('permintaan', $permintaan)
            ->with('mahasiswa', $mahasiswa)
            ->with('skripsi', $skripsi);
    }

    public function store(Requests\PermintaanRequest $request, Authenticatable $user)
    {
        $create = new CreateFTP(
            $request->input('ftp.username'),
            $request->input('ftp.password')
        );
        $create->run();

        $create = new CreateDB(
            $request->input('database.username'),
            $request->input('database.password'),
            $request->input('database.dbname')
        );
        $create->run();

        $permintaan                 = Permintaan::find($request->input('id'));

        QuotaTool::uid($request->input('ftp.username'))->limit($permintaan->kapasitas . 'M')->run();

        \Log::info(QuotaTool::raw());

        $permintaan->status         = 'active';
        $permintaan->password_ftp   = $request->input('ftp.password');
        $permintaan->password_db    = $request->input('database.password');
        $permintaan->user_id        = $user->id;
        $permintaan->activated_at   = Carbon::now();
        $permintaan->expired_at     = Carbon::today()->addMonths(8)->endOfDay();
        $permintaan->save();

        $mahasiswa  = $permintaan->mahasiswa;
        $skripsi    = $mahasiswa->skripsi;

        $mahasiswa->store($request->input('mahasiswa'));
        $skripsi->store($request->input('skripsi'));

        $permintaan->setActive(); // Sendmail inside

        return redirect()->route('backend.permintaan.show', $permintaan->id);
    }

    public function edit($id)
    {
        $permintaan = Permintaan::find($id);
        $mahasiswa  = $permintaan->mahasiswa;
        $skripsi    = $mahasiswa->skripsi;

        return view('page.permintaan.edit')
            ->with('page_title', 'Edit Akun')
            ->with('permintaan', $permintaan)
            ->with('mahasiswa', $mahasiswa)
            ->with('skripsi', $skripsi);
    }

    public function update(Requests\UpdateSkripsiRequest $request, $id)
    {
        $permintaan = Permintaan::find($id);
        $mahasiswa  = $permintaan->mahasiswa;
        $skripsi    = $mahasiswa->skripsi;

        $ftp = new CreateFTP(
            $request->input('ftp.username'),
            $request->input('ftp.password')
        );

        $ftp->setPassword($request->input('ftp.password'));

        $permintaan->password_ftp   = $request->input('ftp.password');
        $permintaan->kapasitas      = $request->input('ftp.quota');
        $permintaan->save();

        QuotaTool::uid($permintaan->username)->limit($permintaan->kapasitas . 'M')->run();

        \Log::info(QuotaTool::raw());

        $mahasiswa->store($request->input('mahasiswa'));
        $skripsi->store($request->input('skripsi'));

        return redirect()->route('backend.permintaan.edit', $id)->with('success', 'Permintaan berhasil diperbarui');
    }

    public function suspend($id)
    {
        $permintaan = Permintaan::find($id);

        if ($permintaan->active) {
            $permintaan->setSuspend();

            return redirect()->route('backend.permintaan.show', $permintaan->id)->withSuccess('Akun berhasil disuspend.');
        }

        return redirect()->route('backend.permintaan.show', $permintaan->id)->withError('Akun belum diaktifkan.');
    }

    public function unsuspend($id)
    {
        $permintaan = Permintaan::find($id);
        $permintaan->setUnsuspend();

        return redirect()->route('backend.permintaan.show', $permintaan->id);
    }

    public function destroyConfirm($id)
    {
        $permintaan = Permintaan::find($id);
        $mahasiswa  = $permintaan->mahasiswa;
        $skripsi    = $mahasiswa->skripsi;

        return view('page.permintaan.destroy_confirm')
            ->with('page_title', 'Konfirmasi Hapus Akun')
            ->with('permintaan', $permintaan)
            ->with('mahasiswa', $mahasiswa)
            ->with('skripsi', $skripsi);
    }

    public function sendMailActivation($id)
    {
        $permintaan = Permintaan::find($id);

        event(new Activated($permintaan));

        return redirect()->route('backend.permintaan.show', $id);
    }

    public function destroy(Request $request, $id)
    {
        $permintaan = Permintaan::find($id);
        $mahasiswa  = $permintaan->mahasiswa;
        $skripsi    = $mahasiswa->skripsi;
        $user       = $mahasiswa->user;

        $destroy_ftp    = $request->input('destroy_ftp');
        $destroy_db     = $request->input('destroy_db');

        if ($destroy_ftp) {
            $ftp = new CreateFTP(
                $permintaan->username,
                $permintaan->password_ftp
            );
            $ftp->down();
        }

        if ($destroy_db) {
            $db = new CreateDB(
                $permintaan->username,
                $permintaan->password_db,
                $permintaan->db_name
            );
            $db->down();
        }

        $skripsi->delete();
        $mahasiswa->delete();
        $permintaan->delete();
        $user->delete();

        return redirect()->route('backend.permintaan.index')->with('success', 'Akun berhasil dihapus');
    }

}
