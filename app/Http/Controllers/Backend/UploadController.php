<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests;

use Illuminate\Http\Request;

class UploadController extends Controller {

	public function form()
    {
        return view('page.upload.form')->withPageTitle('Upload PDF');
    }

    public function submit(Request $request)
    {
        $panduan            = false;
        $instruksi_kerja    = false;

        if ($request->hasFile('panduan')) {
            $file = $request->file('panduan');
            $file->move(public_path('upload'), 'panduan.pdf');
        }

        if ($request->hasFile('instruksi_kerja')) {
            $file = $request->file('instruksi_kerja');
            $file->move(public_path('upload'), 'instruksi_kerja.pdf');
        }

        return redirect()->route('backend.upload.form')->withSuccess('PDF berhasil di upload.');
    }

}
