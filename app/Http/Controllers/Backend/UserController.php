<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;

use App\Http\Requests\UserRequest;
use App\User;
use App\Profile;
use App\Permintaan;

use Input;
use Hash;

use Navigator;

class UserController extends Controller {

	public function __construct()
	{
		parent::__construct();

        Navigator::setActive(route('backend.user.index'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Authenticatable $user)
	{
		if ($user->level == 0) {
			$users = User::onlyUser()->paginate();

			return view('page.user.index')->with('page_title', 'User')->with('users', $users);
		} else {
			return $this->edit($user->id);
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('page.user.create')->with('page_title', 'Create User');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UserRequest $request)
	{
		$user = new User;
		$user->name		= $request->input('name');
		$user->email	= $request->input('email');
		$user->password	= $request->input('password');
		$user->status	= $request->input('status', 1);
		$user->level	= $request->has('level') ? $request->input('level', 1) : 1;
		$user->save();

		$profile	= new Profile;
		$profile->first_name	= $request->input('profile.first_name');
		$profile->last_name		= $request->input('profile.last_name');
		$profile->address		= $request->input('profile.address');
		$profile->phone_number	= $request->input('profile.phone_number');
		$profile->user_id		= $user->id;
		$profile->save();

		return redirect()->route('backend.user.edit', $user->id)->with('success', 'User berhasil dibuat');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);

		$profile = $user->profile;

		return view('page.user.edit')->with('user', $user)->with('profile', $profile)->with('page_title', 'Edit User');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$user 		= User::find($id);
		$user->store();

		$profile	= Profile::find($user->profile->id);
		$profile->store(Input::get('profile'));

		return redirect()->route('backend.user.edit', $id)->with('success', 'User berhasil diperbarui');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, Authenticatable $auth)
	{
		$user = User::find($id);

		Permintaan::migrateUser($user, $auth);

		$user->profile->delete();
		$user->delete();

		return redirect()->route('backend.user.index')->with('success', 'User berhasil dihapus');
	}

	public function password($id)
	{
		$user = User::find($id);

		return view('page.user.change_password', ['user' => $user]);
	}

	public function change(Requests\ChangePasswordRequest $request, $id)
	{
		$user 			= User::find($id);
		$user->password	= Hash::make($request->input('password'));
		$user->save();

		return redirect()->route('backend.user.edit', $id)->with('success', 'Password berhasil diperbarui');
	}

}
