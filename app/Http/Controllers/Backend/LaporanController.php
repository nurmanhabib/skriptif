<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests;

use Illuminate\Http\Request;

use App\Permintaan;
use App\Http\Requests\LaporanRequest;
use PDF_MC_Table;

use Carbon\Carbon;

class LaporanController extends Controller {

	public function getIndex()
    {
        return view('page.laporan.index')
        ->with('page_title', 'Laporan');
    }

    public function postDownload(LaporanRequest $request)
    {
        $filter_info    = '';
        $permintaan     = Permintaan::latest();

        if ($request->has('daterange')) {
            $datetime_input = $request->input('daterange');
            $datetime_expld = explode(' - ', $datetime_input);
            $datetime_start = $datetime_expld[0];
            $datetime_end   = $datetime_expld[1];

            $start  = Carbon::createFromFormat('Y/m/d', $datetime_start)->startOfDay();
            $end    = Carbon::createFromFormat('Y/m/d', $datetime_end)->endOfDay();

            $permintaan->dateRange($start, $end);
            $filter_info .= '_' . $datetime_input;
        }

        if ($request->has('angkatan')) {
            $permintaan->angkatan($request->angkatan);
            $filter_info .= '_Angkatan_' . $request->angkatan;
        }

        if ($request->has('status')) {
            $status = $request->input('status');

            if (array_key_exists('active', $status)) {
                $permintaan->active();
                $filter_info .= '_active';
            }

            if (array_key_exists('pending', $status)) {
                $permintaan->pending();
                $filter_info .= '_pending';
            }
        }

        $get_permintaan = $permintaan->get();

        $fpdf = new PDF_MC_Table;
        $fpdf->AddPage('A4', null, 'L');
        $fpdf->SetTitle('UAD-TIF-SKRIPSI');

        // HEADING
        $fpdf->SetFont('Arial', 'B', 14);
        $fpdf->Cell(0, 6, 'LAPORAN PERMINTAAN HOSTING SKRIPSI TIF', 0, 1, 'C');
        $fpdf->Cell(0, 6, 'UNIVERSITAS AHMAD DAHLAN', 0, 1, 'C');

        // BODY
        $fpdf->Ln();
        $fpdf->SetFont('Arial', 'B', 10);

        $width = [10, 20, 40, 70, 40, 45, 25, 25];

        $fpdf->SetWidths($width);
        $fpdf->Row(['No.', 'NIM', 'Nama', 'Judul Skripsi', 'Dosen Pembimbing', 'No. HP/Email', 'Waktu Daftar', 'Waktu Aktivasi']);

        $fpdf->SetFont('Arial', '', 10);

        $i = 1;

        foreach ($get_permintaan as $permintaan) {
            $mahasiswa  = $permintaan->mahasiswa;
            $skripsi    = $mahasiswa->skripsi;

            $fpdf->Row([
                $i,
                $mahasiswa->nim,
                $mahasiswa->nama,
                $skripsi->judul,
                $skripsi->dosen_pembimbing,
                $mahasiswa->no_hp . " / " . $mahasiswa->email,
                $permintaan->created_at,
                $permintaan->active ? $permintaan->activated_at : '-',
            ]);

            $i++;
        }

        $timestamps = Carbon::now();

        $fpdf->Output($timestamps . "_UAD-TIF-SKRIPSI" . $filter_info . ".pdf", 'D');
        exit;
    }

}
