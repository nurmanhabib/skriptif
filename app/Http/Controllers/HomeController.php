<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;

use App\Permintaan;
use App\Skripsi;

class HomeController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $skripsi    = Skripsi::activeLatest();
        $page_title = 'Judul Skripsi Mahasiswa';

        if ($request->has('cari')) {
            $skripsi    = $skripsi->cari($request->get('cari'));
            $page_title = 'Cari Skripsi <i>"'.$request->get('cari').'"</i>';
        }

        $skripsi = $skripsi->paginate(5);

        return view('page.welcome', ['skripsi' => $skripsi])->with('page_title', $page_title);
    }

}
