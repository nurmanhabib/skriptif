<?php namespace App\Http\Controllers\Mahasiswa;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Storage;
use App\Mahasiswa;

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local as Adapter;

use Illuminate\Pagination\LengthAwarePaginator;

class JurnalController extends Controller {

    public function __construct(Request $request)
    {
        $this->base_path    = config('skriptif.dir.jurnal');
        $this->dir          = $request->has('dir') ? '/' . $request->get('dir') : '';
        $this->filename     = $request->has('filename') ? $request->get('filename') : '';
        $this->with_dir     = true;
        $this->with_file    = true;
        $this->extensions   = [];
        $this->request      = $request;

        $this->filesystem   = new Filesystem(new Adapter($this->base_path));
        $this->contents     = $this->filesystem->listContents($this->dir);
    }

    public function show($dir = '')
    {
        if ($this->filesystem->has($dir)) {
            $this->with_dir     = false;
            $this->with_file    = true;
            $this->contents     = $this->filesystem->listContents($dir);
            $files              = $this->filterContents();

            $this->with_dir     = true;
            $this->with_file    = false;
            $this->contents     = $this->filesystem->listContents($dir);
            $dirs               = $this->filterContents();
        } else {
            $files  = [];
            $dirs   = [];
        }

        $files      = collect($files);
        $dirs       = collect($dirs);
        $count      = $dirs->count();
        $perpage    = 2;

        $dirs       = $dirs->forPage($this->request->get('page', 1), $perpage);

        $dirs       = new LengthAwarePaginator($dirs, $count, $perpage);
        $dirs->setPath(route('jurnal.show'));

        return view('page.download.jurnal')->withPageTitle('Download')
        ->withFiles($files)
        ->withDirs($dirs);
    }

    public function download(Request $request, $dir = '')
    {
        $filename   = $request->get('filename');
        $path       = $dir . DIRECTORY_SEPARATOR . $filename;

        if ($this->filesystem->has($path))
            return response()->download($this->filesystem->getAdapter()->applyPathPrefix($path));
        else
            return redirect()->route('home')->withError('File tidak ditersedia untuk Anda.');
    }

    protected function filterContents($contents = [], $overriding = true)
    {
        $contents = $contents ?: $this->contents;

        $contents = array_filter($contents, function($content)
        {
            // Filter dir
            if ($content['type'] == 'dir') {
                if (!$this->with_dir)
                    return false;
            }

            // Filter file
            else
                if (!$this->with_file)
                    return false;
                elseif (!empty($this->extensions))
                    if (!in_array($content['extension'], $this->extensions))
                        return false;
                        return true;
        });

        if ($overriding)
            $this->contents = $contents;

        return $contents;
    }

}
