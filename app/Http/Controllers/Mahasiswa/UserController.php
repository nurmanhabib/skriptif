<?php namespace App\Http\Controllers\Mahasiswa;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Contracts\Auth\Authenticatable;

use App\Http\Requests\ChangePasswordRequest;

class UserController extends Controller {

	public function __construct(Authenticatable $user)
	{
		$this->user = $user;
	}

	public function edit()
	{
		$profile = $this->user->profile;
		$permintaan = $profile->permintaan;
		$skripsi = $profile->skripsi;

		if (!empty($permintaan)) {
			view()->share('permintaan', $permintaan);
			view()->share('skripsi', $skripsi);
		}

		return view('page.mahasiswa.edit')->withPageTitle('Profile')
		->withUser($this->user)
		->withProfile($this->user->profile);
	}

	public function update(Request $request)
	{
		$profile = $this->user->profile;
		$profile->store($request->input('profile'), ['email' => $request->input('email')]);

		$this->user->store($request->only('email'));

		return redirect()->route('mahasiswa.edit')->withSuccess('Profil berhasil diperbarui.');
	}

	public function password()
	{
		return view('page.mahasiswa.change_password')->withPageTitle('Change Password')
		->withUser($this->user);
	}

	public function change(ChangePasswordRequest $request)
	{
		return redirect()->route('mahasiswa.edit')->withSuccess('Password berhasil diperbarui.');
	}

}
