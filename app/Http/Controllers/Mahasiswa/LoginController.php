<?php namespace App\Http\Controllers\Mahasiswa;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\LoginRequest;

class LoginController extends Controller {

	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	public function index()
    {
        return view('page.login_mahasiswa')->with('page_title', 'Login');
    }

    public function submit(LoginRequest $request)
    {
        $credentials = [
            'name'      => $request->input('username'),
            'password'  => $request->input('password'),
            'level'		=> 2,
        ];

        if ($this->auth->attempt($credentials)) {
            return redirect()->intended(route('mahasiswa.edit'));
        } else {
            return redirect()->route('mahasiswa.login')->withInput()->with('error', 'Username atau password salah.');
        }
    }

    public function logout()
    {
        $this->auth->logout();

        return redirect('/');
    }

}
