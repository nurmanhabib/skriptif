<?php namespace App\Http\Controllers\Mahasiswa;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Storage;
use App\Mahasiswa;

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local as Adapter;

class DocumentController extends Controller {

    public function __construct(Request $request)
    {
        $this->base_path    = config('skriptif.dir.mahasiswa');
        $this->dir          = $request->has('dir') ? '/' . $request->get('dir') : '';
        $this->filename     = $request->has('filename') ? $request->get('filename') : '';
        $this->with_dir     = false;
        $this->with_file    = true;
        $this->extensions   = [];

        $this->filesystem   = new Filesystem(new Adapter($this->base_path));
        $this->contents     = $this->filesystem->listContents($this->dir);
    }

    public function show($nim)
    {
        $mahasiswa  = Mahasiswa::nim($nim);
        $permintaan = $mahasiswa->permintaan;
        $dir        = $permintaan->username . DIRECTORY_SEPARATOR . 'doc';

        if ($this->filesystem->has($dir)) {
            $this->contents = $this->filesystem->listContents($dir);
            $this->contents = $this->filterContents();
        } else {
            $this->contents = [];
        }

        return view('page.download.skripsi')->withPageTitle('Download Skripsi: ' . $mahasiswa->nim)
        ->withFiles($this->contents)
        ->withMahasiswa($mahasiswa)
        ->withNim($nim);
    }

    public function pdf($nim, $filename)
    {
        $mahasiswa  = Mahasiswa::nim($nim);
        $permintaan = $mahasiswa->permintaan;
        $dir        = $permintaan->username;
        $path       = $dir . DIRECTORY_SEPARATOR . 'doc' . DIRECTORY_SEPARATOR . $filename;

        if ($this->filesystem->has($path))
            return response()->download($this->filesystem->getAdapter()->applyPathPrefix($path));
        else
            return redirect()->route('home')->withError('File tidak ditersedia untuk Anda.');
    }

    protected function filterContents($contents = [], $overriding = true)
    {
        $contents = $contents ?: $this->contents;

        $contents = array_filter($contents, function($content)
        {
            // Filter dir
            if ($content['type'] == 'dir') {
                if (!$this->with_dir)
                    return false;
            }

            // Filter file
            else
                if (!$this->with_file)
                    return false;
                elseif (!empty($this->extensions))
                    if (!in_array($content['extension'], $this->extensions))
                        return false;
                        return true;
        });

        if ($overriding)
            $this->contents = $contents;

        return $contents;
    }

}
