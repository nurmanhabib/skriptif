<?php namespace App\Http\Controllers\Mahasiswa;

class FolderTrait {

    public function renderDir()
    {
        $this->with_dir = true;
        $this->with_file = false;

        $dir = $this->filterContents();
    }

}