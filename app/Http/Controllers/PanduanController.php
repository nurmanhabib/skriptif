<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PanduanController extends Controller {

	public function getIndex()
	{
		return view('page.panduan.index')->with('page_title', 'Panduan');
	}

}
