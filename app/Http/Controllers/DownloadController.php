<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Response;
use Storage;

use ScanDir;
use Download;

class DownloadController extends Controller {

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

	public function getIndex()
    {
        $download = new Download($this->request);

        return view('page.download.index', ['download' => $download])
        ->with('page_title', 'Jurnal Download');
    }

    public function getFile()
    {
        $file = $this->request->get('filename');

        if (Storage::disk('jurnal')->exists($file)) {
            $full_path  = storage_path() . '/app/jurnal/' . $file;

            return response()->download($full_path);
        }
    }

    public function postFile()
    {
        
    }

}
