<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests\LoginRequest;

use Auth;

class LoginController extends Controller {

	public function index()
    {
        return view('page.login')->with('page_title', 'Login');
    }

    public function submit(LoginRequest $request)
    {
        $credentials = [
            'name'      => $request->input('username'),
            'password'  => $request->input('password')
        ];

        if (Auth::attempt($credentials)) {
            return redirect()->intended(route('backend'));
        } else {
            return redirect()->route('login')->withInput()->with('error', 'Username atau password salah.');
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }

}
