<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Authenticatable;

use App\User;

class RedirectIfRegistered {

	public function __construct(Authenticatable $user = null)
	{
		$this->user = $user ?: new User;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($mahasiswa = $this->user->profile)
			if ($mahasiswa->permintaan)
				return redirect()->route('mahasiswa.edit')->withError('<strong>Maaf,</strong> setiap mahasiswa hanya diperbolehkan memiliki 1 akun. Untuk mendaftar mahasiswa lain, Anda harus melakukan logout terlebih dahulu.');

		return $next($request);
	}

}
