<?php namespace App;

class Mahasiswa extends Model {

	protected $table = 'mahasiswa';

    public static function boot()
    {
        parent::boot();

        Mahasiswa::creating(function($mahasiswa)
        {
            $user = new User;
            $user->name     = $mahasiswa->nim;
            $user->password = $mahasiswa->password;
            $user->email    = $mahasiswa->email;
            $user->status   = 1;
            $user->level    = 2;
            $user->save();

            $mahasiswa->user_id = $user->id;

            unset($mahasiswa->password);
        });
    }

    public function getFullNameAttribute()
    {
        return $this->nama;
    }

    public function permintaan()
    {
        return $this->hasOne('App\Permintaan');
    }

    public function skripsi()
    {
        return $this->hasOne('App\Skripsi');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function scopeNim($query, $nim)
    {
        return $query->where('nim', '=', $nim)->first();
    }

}
