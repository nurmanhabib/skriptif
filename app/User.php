<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public static function boot()
	{
		parent::boot();

		User::creating(function($user)
		{
			$user->password = bcrypt($user->password);
		});
	}

	public function profile()
	{
		if ($this->level == 2)
			return $this->hasOne('App\Mahasiswa');
			return $this->hasOne('App\Profile');
	}

	public function isAdmin()
	{
		return in_array($this->attributes['level'], [0, 1]);
	}

	public function scopeOnlyUser($query)
	{
		return $query->select($this->getTable() . '.*')
		->rightJoin('profiles', 'profiles.user_id', '=', $this->getTable() . '.id');
	}

	public function scopeOnlyMahasiswa($query)
	{
		return $query->select($this->getTable() . '.*')
		->rightJoin('mahasiswa', 'mahasiswa.user_id', '=', $this->getTable() . '.id');
	}

	public function scopeName($query, $name)
	{
		return $query->where('name', $name);
	}

	public function scopeEmail($query, $email)
	{
		return $query->where('email', $email);
	}

	public function scopeNameOrEmail($query, $name, $email)
	{
		return $query->where('name', $name)->orWhere('email', $email);
	}

}
