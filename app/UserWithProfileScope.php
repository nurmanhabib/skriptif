<?php namespace App;

use Illuminate\Database\Eloquent\ScopeInterface;
use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Database\Eloquent\Builder;

class UserWithProfileScope implements ScopeInterface {

    public function apply(Builder $builder, BaseModel $model)
    {
        $builder->select('users.*')->rightJoin('profiles', 'profiles.user_id', '=', 'users.id');
    }

    public function remove(Builder $builder, BaseModel $model)
    {
       $query = $builder->getQuery();

       $query->joins = [];

       // here you remove the where close to allow developer load   
       // without your global scope condition
   
    }

} 