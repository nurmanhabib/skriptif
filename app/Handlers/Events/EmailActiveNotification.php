<?php namespace App\Handlers\Events;

use App\Events\PermintaanActivated;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

use Mail;
use Session;

class EmailActiveNotification {

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  PermintaanUnsuspended  $event
	 * @return void
	 */
	public function handle(PermintaanActivated $event)
	{
		$permintaan	= $event->permintaan;
		$mahasiswa 	= $event->mahasiswa;
        
        Mail::send('emails.permintaan_active', ['mahasiswa' => $mahasiswa, 'permintaan' => $permintaan], function($message) use($mahasiswa, $permintaan)
        {
            $message->to($mahasiswa->email, $mahasiswa->nama)->subject('Akun Aktif Skripsi TIF UAD');
        });

        Session::flash('success', 'Email aktivasi berhasil dikirim ke <strong>'.$mahasiswa->email.'</strong>');
	}

}
