<?php namespace App\Handlers\Events;

use App\Events\PermintaanSuspended;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

use Session;
use Mail;

class EmailSuspendNotification {

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  PermintaanSuspended  $event
	 * @return void
	 */
	public function handle(PermintaanSuspended $event)
	{
		$permintaan	= $event->permintaan;
		$mahasiswa 	= $event->mahasiswa;
        
        Mail::send('emails.permintaan_suspend', ['mahasiswa' => $mahasiswa, 'permintaan' => $permintaan], function($message) use($mahasiswa, $permintaan)
        {
            $message->to($mahasiswa->email, $mahasiswa->nama)->subject('Akun Suspend Skripsi TIF UAD');
        });

        Session::flash('success', 'Akun berhasil di suspend. Email suspend berhasil dikirim ke <strong>'.$mahasiswa->email.'</strong>');
	}

}
