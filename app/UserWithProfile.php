<?php namespace App;

trait UserWithProfile {

	public static function bootUserWithProfile()
	{
		static::addGlobalScope(new UserWithProfileScope);
	}

	public static function withMahasiswa()
	{
		return (new static)->newQueryWithoutScope(new UserWithProfileScope);
	}

	public static function onlyMahasiswa()
	{
		return self::withMahasiswa()->rightJoin('mahasiswa', 'mahasiswa.user_id', '=', 'users.id');
	}

}