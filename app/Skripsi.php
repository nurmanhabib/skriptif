<?php namespace App;

class Skripsi extends Model {

	protected $table = 'skripsi';

    public function getLinkAttribute()
    {
        return url($this->mahasiswa->nim);
    }

    public function getTahunAttribute()
    {
        return $this->created_at->year;
    }

    public function mahasiswa()
    {
        return $this->belongsTo('App\Mahasiswa');
    }

    public function scopeActiveLatest($query)
    {
    	return $query->join('mahasiswa', 'mahasiswa.id', '=', $this->getTable() . '.mahasiswa_id')
    				->join('permintaan', 'permintaan.mahasiswa_id', '=', 'mahasiswa.id')
    				->where('permintaan.status', '=', 'active')
    				->orderBy('permintaan.created_at', 'desc');
    }

    public function scopeCari($query, $key)
    {
        return $query->where('skripsi.judul', 'like', '%' . $key . '%')
                    ->orWhere('mahasiswa.nama', 'like', '%' . $key . '%')
                    ->orWhere('mahasiswa.nim', 'like', $key);
    }

}
