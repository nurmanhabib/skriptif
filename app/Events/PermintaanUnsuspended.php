<?php namespace App\Events;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class PermintaanUnsuspended extends Event {

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct($permintaan)
	{
		$this->permintaan	= $permintaan;
		$this->mahasiswa	= $permintaan->mahasiswa;
	}

}
