<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\Permintaan;
use CreateFTP;
use Log;

class SkriptifSuspend extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'skriptif:expired';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Mencari akun yang sudah lewat dari suspend.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$permintaan	= Permintaan::activeExpired();
		$count		= $permintaan->count();

		foreach ($permintaan->get() as $account)
			$account->setSuspend();

		if ($count) {
	    	
	    	$this->info('Sistem telah melakukan suspend terhadap ' . $count . ' akun.');			
			Log::info('Sistem telah melakukan suspend terhadap ' . $count . ' akun.');

		} else {

	    	$this->info('Tidak ada akun yang expired.');	
			Log::info('Tidak ada akun yang expired.');

		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [];
	}

}
