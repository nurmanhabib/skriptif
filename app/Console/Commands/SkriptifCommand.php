<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\Mahasiswa;
use App\Permintaan;
use App\User;
use CreateFTP;

class SkriptifCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'skriptif:upgrade';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $mahasiswa  = Mahasiswa::where('user_id', 0);
        $count      = $mahasiswa->count();

        foreach ($mahasiswa->get() as $mhs) {
            $user           = User::nameOrEmail($mhs->nim, $mhs->email)->first();
            $user           = !empty($user) ?: new User;
            $user->name     = $mhs->nim;
            $user->email    = $mhs->email;
            $user->password = 'password';
            $user->status   = true;
            $user->level    = 2;
            $user->save();

            $mhs->user_id   = $user->id;
            $mhs->save();
        }

        $this->info($count . ' Mahasiswa sudah diupgrade.');

        $this->upgradeFtp();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }

    public function upgradeFtp()
    {
        $permintaan = Permintaan::active()->get();

        foreach ($permintaan as $akun) {
            $ftp = new CreateFTP($akun->username, $akun->password_ftp);
            $ftp->run();
        }

        $this->info($permintaan->count() . ' akun FTP sudah diupgrade.');
    }

}
