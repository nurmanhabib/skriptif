<?php namespace App;

use CreateFTP;
use CreateDB;
use Carbon\Carbon;

use App\Events\PermintaanActivated as Activated;
use App\Events\PermintaanSuspended as Suspended;
use App\Events\PermintaanUnsuspended as Unsuspended;

use Nurmanhabib\QuotaTool\Facades\QuotaTool;

class Permintaan extends Model {

    protected $table = 'permintaan';
    protected $dates = ['activated_at', 'expired_at'];

    public function mahasiswa()
    {
        return $this->belongsTo('App\Mahasiswa');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getStatusLabelAttribute()
    {
        $labels = [
            'pending'   => 'warning',
            'active'    => 'success',
        ];

        $label = array_get($labels, $this->status, 'default');

        return '<span class="label label-'.$label.'">'.$this->status.'</span>';
    }

    public function getUsernameAttribute()
    {
        return 'u' . $this->mahasiswa->nim;
    }

    public function getPasswordAttribute($value)
    {
        $chars      = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $password   = substr( str_shuffle( $chars ), 0, 8 );

        return $value ?: $password;
    }

    public function getDbNameAttribute()
    {
        return "db_" . $this->mahasiswa->nim;
    }

    public function getActiveAttribute()
    {
        return $this->status == 'active';
    }

    public function getSuspendAttribute()
    {
        return $this->status == 'suspend';
    }

    public function getCurrentAttribute()
    {
        $dumping    = QuotaTool::uid($this->username)->dumping();
        $current    = array_get($dumping, 'block.current', 0) / 1024;
        
        return round($current, 2);
    }

    public function getCurrentPercentAttribute()
    {
        $current    = $this->current;
        $percent    = ($current / $this->kapasitas) * 100;
        
        return $percent;
    }

    public function getProgress()
    {
        return view('progress', ['permintaan' => $this]);
    }

    public function setActive()
    {
        $this->status = 'active';
        $this->save();

        event(new Activated($this));
    }

    public function setSuspend()
    {
        $ftp = new CreateFTP(
            $this->username,
            $this->password_ftp
        );

        $db = new CreateDB(
            $this->username,
            $this->password_db,
            $this->db_name
        );
        
        $ftp->suspend();
        $db->suspend();

        $this->status = 'suspend';
        $this->save();

        event(new Suspended($this));
    }

    public function setUnsuspend()
    {
        $ftp = new CreateFTP(
            $this->username,
            $this->password_ftp
        );

        $db = new CreateDB(
            $this->username,
            $this->password_db,
            $this->db_name
        );
        
        $ftp->unsuspend();
        $db->unsuspend();

        $this->status = 'active';
        $this->save();

        event(new Activated($this));
    }

    public function scopeMigrateUser($query, User $from, User $to)
    {
        return $query->userId($from->id)->update(['user_id' => $to->id]);
    }

    public function scopeUserId($query, $user_id)
    {
        return $query->where('user_id', '=', $user_id);
    }

    public function scopeStatus($query, $status)
    {
        return $query->orWhere('status', '=', $status);
    }

    public function scopePending($query)
    {
        return $this->scopeStatus($query, 'pending');
    }

    public function scopeActive($query)
    {
        return $this->scopeStatus($query, 'active');
    }

    public function scopeSuspend($query)
    {
        return $this->scopeStatus($query, 'suspend');
    }

    public function scopeActiveExpired($query)
    {
        $now = Carbon::today();

        return $this->active()->where('expired_at', '<=', $now);
    }

    public function scopeDateRange($query, $start, $end)
    {
        return $query->where('created_at', '>=', $start)->where('created_at', '<=', $end);
    }

    public function scopeAngkatan($query, $tahun)
    {
        $angkatan   = substr($tahun, -2);

        return $query->select($this->getTable() . '.*')
                    ->join('mahasiswa', $this->getTable() . '.mahasiswa_id', '=', 'mahasiswa.id')
                    ->where('mahasiswa.nim', 'like', $angkatan . '%');
    }

    public function scopeLatestActivated($query)
    {
        return $query->orderBy('activated_at', 'desc');
    }

    public function scopeCari($query, $key)
    {
        return $query->select('permintaan.*')
                    ->join('mahasiswa', $this->getTable() . '.mahasiswa_id', '=', 'mahasiswa.id')
                    ->join('skripsi', 'mahasiswa.id', '=', 'skripsi.mahasiswa_id')
                    ->where('skripsi.judul', 'like', '%' . $key . '%')
                    ->orWhere('mahasiswa.nama', 'like', '%' . $key . '%')
                    ->orWhere('mahasiswa.nim', 'like', $key);
    }

}
