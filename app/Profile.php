<?php namespace App;

class Profile extends Model {

	public function user()
    {
        return $this->belongsTo('User');
    }

    public function getFullNameAttribute()
    {
        return trim($this->first_name . ' ' . $this->last_name);
    }

}
