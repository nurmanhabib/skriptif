
/*Kelulusan*/
		$(function () {
		    var chart;
		    
		    $(document).ready(function () {
		    	
		    	// Build the chart
		        $('#kelulusan').highcharts({
		            chart: {
		                plotBackgroundColor: null,
		                plotBorderWidth: null,
		                plotShadow: false
		            },
		            title: {
		                text: ''
		            },
		            tooltip: {
		        	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		            },
		            plotOptions: {
		                pie: {
		                    allowPointSelect: true,
		                    cursor: 'pointer',
		                    dataLabels: {
		                        enabled: false
		                    },
		                    showInLegend: true
		                }
		            },
		            series: [{
		                type: 'pie',
		                name: 'Tingkat Kelulusan',
		                data: [
		                    ['2009',   45.0],
		                    ['20110',       26.8],
		                    {
		                        name: '2011',
		                        y: 12.8,
		                        sliced: true,
		                        selected: true
		                    },
		                    ['2012',    8.5],
		                    ['2013',     6.2],
		                ]
		            }]
		        });
		    });
		    
		});

/*Profesi*/
		$(function () {
		    var chart;
		    
		    $(document).ready(function () {
		    	
		    	// Build the chart
		        $('#profesi').highcharts({
		            chart: {
		                plotBackgroundColor: null,
		                plotBorderWidth: null,
		                plotShadow: false
		            },
		            title: {
		                text: ''
		            },
		            tooltip: {
		        	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		            },
		            plotOptions: {
		                pie: {
		                    allowPointSelect: true,
		                    cursor: 'pointer',
		                    dataLabels: {
		                        enabled: false
		                    },
		                    showInLegend: true
		                }
		            },
		            series: [{
		                type: 'pie',
		                name: 'Profesi',
		                data: [
		                	['Apoteker',   35.0],
		                    ['Wirausaha',   15.0],
		                    ['Dosen',       26.0],
		                    {
		                        name: 'Studi Lanjut',
		                        y: 12.8,
		                        sliced: true,
		                        selected: true
		                    },
		                ]
		            }]
		        });
		    });
		    
		});

/*Studi Lanjut*/
		$(function () {
		    var chart;
		    
		    $(document).ready(function () {
		    	
		    	// Build the chart
		        $('#studi').highcharts({
		            chart: {
		                plotBackgroundColor: null,
		                plotBorderWidth: null,
		                plotShadow: false
		            },
		            title: {
		                text: ''
		            },
		            tooltip: {
		        	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		            },
		            plotOptions: {
		                pie: {
		                    allowPointSelect: true,
		                    cursor: 'pointer',
		                    dataLabels: {
		                        enabled: false
		                    },
		                    showInLegend: true
		                }
		            },
		            series: [{
		                type: 'pie',
		                name: 'Studi Lanjut',
		                data: [
		                    ['2009',   15.0],
		                    ['2010',       25.0],
		                    ['2011',    20.5],
		                    ['2012',     19.5],
		                    {
		                        name: '2013',
		                        y: 20.0,
		                        sliced: true,
		                        selected: true
		                    },
		                ]
		            }]
		        });
		    });
		    
		});

/*Gaji*/
		$(function () {
		    var chart;
		    
		    $(document).ready(function () {
		    	
		    	// Build the chart
		        $('#gaji').highcharts({
		            chart: {
		                plotBackgroundColor: null,
		                plotBorderWidth: null,
		                plotShadow: false
		            },
		            title: {
		                text: ''
		            },
		            tooltip: {
		        	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		            },
		            plotOptions: {
		                pie: {
		                    allowPointSelect: true,
		                    cursor: 'pointer',
		                    dataLabels: {
		                        enabled: false
		                    },
		                    showInLegend: true
		                }
		            },
		            series: [{
		                type: 'pie',
		                name: 'Studi Lanjut',
		                data: [
		                	['< 500,000',   0.0],
		                	{
		                        name: '1,500,000-3,000,000',
		                        y: 45.0,
		                        sliced: true,
		                        selected: true
		                    },
		                    ['500,000-1,500,000',   15.0],
		                    ['3,000,000 >',    40.0],
		                ]
		            }]
		        });
		    });
		    
		});