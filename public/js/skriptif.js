$(document).ready(function(){
    $('.btn-delete').click(function(){
        return confirm('Are you sure?')
    })

    $('.daterange').daterangepicker({
        format: 'YYYY/MM/DD',
    })
})