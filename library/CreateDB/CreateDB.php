<?php

class CreateDB {

    protected $data;

    public function __construct($username, $password, $dbname)
    {
        $this->data = ['username' => $username, 'password' => $password, 'dbname' => $dbname];
    }

    public function run()
    {
        $result = DB::select("SELECT * FROM mysql.user WHERE User = '".$this->username."'");

        if (count($result))
            DB::statement("DROP USER '".$this->username."'@'localhost'");
            DB::statement("DROP DATABASE IF EXISTS ".$this->dbname);
        
        DB::statement("CREATE USER '".$this->username."'@'localhost' IDENTIFIED BY '".$this->password."'");
        DB::statement("CREATE DATABASE ".$this->dbname."");
        DB::statement("GRANT ALL ON ".$this->dbname.".* TO '".$this->username."'@'localhost'");
        DB::statement("FLUSH PRIVILEGES;");
    }

    public function down()
    {
        DB::statement("GRANT USAGE ON *.* TO '".$this->username."'@'localhost'");
        DB::statement("DROP USER '".$this->username."'@'localhost'");
        DB::statement("DROP DATABASE IF EXISTS ".$this->dbname);
        DB::statement("FLUSH PRIVILEGES;");
    }

    public function suspend()
    {
        DB::statement("REVOKE ALL PRIVILEGES, GRANT OPTION FROM '".$this->username."'@'localhost'");
        DB::statement("FLUSH PRIVILEGES;");
    }

    public function unsuspend()
    {
        DB::statement("GRANT ALL ON ".$this->dbname.".* TO '".$this->username."'@'localhost'");
        DB::statement("FLUSH PRIVILEGES;");
    }

    public function __get($key)
    {
        return array_get($this->data, $key);
    }

    public function __set($key, $value)
    {
        $this->data[$key] = $value;
    }

}
