<?php

class CreateFTP {

    protected $data;

    public function __construct($username, $password)
    {
        $homedir    = '/home/'.$username;
        $this->data = ['username' => $username, 'password' => $password, 'homedir' => $homedir];
    }

    public function run()
    {
        // Create user linux with home dir
        shell_exec("sudo useradd -d ".$this->homedir." -s /sbin/nologin ".$this->username);

        // Set password user
        shell_exec("sudo ".base_path()."/library/sh/chpasswd ".$this->username." ".$this->password);

        // Add user to user_list VSFTPD
        $this->unsuspend();

        // Create directory for user FTP
        shell_exec("sudo mkdir ".$this->homedir);
        shell_exec("sudo chown root.root ".$this->homedir);

		// Create sub directory public_html
		shell_exec("sudo mkdir ".$this->homedir."/public_html");
		shell_exec("sudo cp -r ".base_path() ."/resources/skriptif/samples/public_html/* ".$this->homedir."/public_html");
		shell_exec("sudo chown -R ".$this->username.".root ".$this->homedir."/public_html");

		// Create sub directory doc
		shell_exec("sudo mkdir ".$this->homedir."/doc");
		shell_exec("sudo chown -R ".$this->username.".root ".$this->homedir."/doc");
    }

    public function down()
    {
        $this->suspend();

        shell_exec("sudo userdel ".$this->username);
        shell_exec("sudo rm -r ".$this->homedir);
    }

    public function suspend()
    {
        shell_exec("sudo ".base_path()."/library/sh/del_userlist ".$this->username);
    }

    public function unsuspend()
    {
        $this->suspend();
        
        shell_exec("sudo ".base_path()."/library/sh/add_userlist ".$this->username);
        shell_exec("sudo ".base_path()."/library/sh/chpasswd ".$this->username." ".$this->password_ftp);
    }

    public function setPassword($password)
    {
        shell_exec("sudo ".base_path()."/library/sh/chpasswd ".$this->username." ".$password);
    }

    public function __get($key)
    {
        return array_get($this->data, $key);
    }

    public function __set($key, $value)
    {
        $this->data[$key] = $value;
    }

}
