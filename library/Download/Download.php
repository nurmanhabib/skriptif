<?php

use Illuminate\Http\Request;

class Download {

    public function __construct(Request $request)
    {      
        $this->dir      = $request->has('dir') ? $request->get('dir') : ''; 
        $this->folders  = Storage::disk('jurnal')->directories($this->dir);
        $this->files    = Storage::disk('jurnal')->allFiles($this->dir);
    }

    public function up()
    {
        $dir    = $this->dir;
        $updir  = '';

        if ($dir) {
            $explode    = explode('\\', $dir);

            if ($count = count($explode))
                unset($explode[$count-1]);

            $updir      = implode('\\', $explode);
        }

        return $updir;
    }

    public function getFolders()
    {
        $folders = [];

        foreach ($this->folders as $folder) {
            $explode            = explode('\\', rtrim($folder, '\\'));
            $info['fullpath']   = $folder;
            $info['path']       = $this->dir;
            $info['foldername'] = end($explode);

            array_push($folders, $info);
        }

        return collect($folders);
    }

    public function getFiles()
    {
        $files = [];

        foreach ($this->files as $file) {
            $explode            = explode('\\', rtrim($file, '\\'));
            $info['fullpath']   = $file;
            $info['path']       = $this->dir;
            $info['filename']   = end($explode);

            array_push($files, $info);
        }

        return collect($files);
    }

    public function renderUp()
    {
        $result = '';

        if ($this->dir) {
            $param  = ['dir' => $this->up()];
            $query  = http_build_query($param);
            $link   = action('DownloadController@getIndex', $query);
            $anchor = '<i class="fa fa-fw fa-level-up"></i> ..';
            $result .= '<li><a href="' . $link . '">' . $anchor . '</a></li>';
        }

        return $result;
    }

    public function renderFolders()
    {
        $result = '';

        foreach ($this->getFolders() as $folder) {
            $param  = ['dir' => $folder['fullpath']];
            $query  = http_build_query($param);
            $link   = action('DownloadController@getIndex', $query);
            $anchor = '<i class="fa fa-fw fa-folder"></i> ' . $folder['foldername'];
            $result .= '<li><a href="' . $link . '">' . $anchor . '</a></li>';
        }

        return $result;
    }

    public function render()
    {
        $result = '';

        foreach ($this->getFiles() as $file) {
            $param  = ['filename' => $file['fullpath']];
            $query  = http_build_query($param);
            $link   = action('DownloadController@getFile', '?' . $query);
            $anchor = '<i class="fa fa-fw fa-file"></i> ' . $file['filename'];
            $result .= '<li><a href="' . $link . '" target="_blank">' . $anchor . '</a></li>';
        }

        return $result;
    }

}