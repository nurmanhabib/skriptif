@if(count($errors))
    <div class="alert alert-danger">
        <strong>ERROR!</strong> 

        @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

@if(Session::has('error'))
    <div class="alert alert-danger">
        <p>{!! Session::get('error') !!}</p>
    </div>
@endif

@if(Session::has('success'))
    <div class="alert alert-success">
        <p>{!! Session::get('success') !!}</p>
    </div>
@endif