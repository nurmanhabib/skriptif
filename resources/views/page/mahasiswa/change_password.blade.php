@extends('layout.portal')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Change Password
            </div>

            {!! Form::open(['route' => 'mahasiswa.change', 'class' => 'form-horizontal']) !!}
            {!! Form::hidden('id', $my->id) !!}
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('password', null, ['class' => 'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                {!! Form::password('password', ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('password_confirmation', null, ['class' => 'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('password_old', null, ['class' => 'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                {!! Form::password('password_old', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="panel-footer">
                <button class="btn btn-primary"><i class="fa fa-fw fa-refresh"></i> Update</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection