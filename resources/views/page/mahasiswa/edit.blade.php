@extends('layout.portal')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">

            {!! Form::model($user, ['route' => 'mahasiswa.update', 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
            <div class="panel-body">
                <legend>Edit Profile</legend>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('name', 'Username', ['class' => 'control-label col-md-4']) !!}
                            <div class="col-md-8">
                                {!! Form::text('name', null, ['class' => 'form-control', 'readonly' => true]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('password', null, ['class' => 'control-label col-md-4']) !!}
                            <div class="col-md-8">
                                <a href="{!! route('mahasiswa.password') !!}" class="btn btn-primary">Change password</a>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('email', null, ['class' => 'control-label col-md-4']) !!}
                            <div class="col-md-8">
                                {!! Form::text('email', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('profile[nama]', 'Nama Lengkap', ['class' => 'control-label col-md-4']) !!}
                            <div class="col-md-8">
                                {!! Form::text('profile[nama]', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('profile[no_hp]', 'No. HP', ['class' => 'control-label col-md-4']) !!}
                            <div class="col-md-8">
                                {!! Form::text('profile[no_hp]', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-footer">
                <button class="btn btn-primary">Update</button>
            </div>
            {!! Form::close() !!}
        </div>

        @if(!empty($skripsi))
        <div class="panel panel-default">
            <div class="panel-body form-horizontal">
                <legend>Skripsi</legend>

                <div class="form-group">
                    {!! Form::label('skripsi[judul]', 'Judul', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $skripsi->judul !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('skripsi[dosen_pembimbing]', 'Dosen Pembimbing', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $skripsi->dosen_pembimbing !!}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body form-horizontal">
                <legend>Status</legend>

                <div class="form-group">
                    {!! Form::label('permintaan[status]', 'Status', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $permintaan->status_label !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('permintaan[activated_at]', 'Waktu Aktivasi', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $permintaan->activated_at !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('permintaan[user_id]', 'Diaktivasi oleh', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $permintaan->user ? $permintaan->user->name : '' !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('permintaan[expired_at]', 'Masa Berlaku', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-8">
                        <p class="form-control-static">Aktif sampai <span class="label label-info">{{ $permintaan->expired_at }}</span></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body form-horizontal">
                <legend>Web</legend>

                <div class="form-group">
                    {!! Form::label('homepage', 'Homepage', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! link_to($skripsi->link, null, ['target' => '_blank']) !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('phpmyadmin', 'PHPMyAdmin', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! link_to('phpmyadmin', null, ['target' => '_blank']) !!}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body form-horizontal">
                <div class="row">
                    <div class="col-md-6">
                        <legend>FTP Account</legend>

                        @if($permintaan->status == 'active')
                        <div class="form-group">
                            {!! Form::label('ftp[username]', 'Username', ['class' => 'control-label col-md-4']) !!}
                            <div class="col-md-8">
                                <p class="form-control-static">{!! $permintaan->username !!}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('ftp[password]', 'Password', ['class' => 'control-label col-md-4']) !!}
                            <div class="col-md-8">
                                <p class="form-control-static"><code>{!! $permintaan->password_ftp !!}</code></p>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('ftp[quota]', 'Quota', ['class' => 'control-label col-md-4']) !!}
                            <div class="col-md-6">
                                <div class="form-control-static">
                                    {!! $permintaan->getProgress() !!}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <p class="form-control-static">
                                    {!! $permintaan->kapasitas !!} MB
                                </p>
                            </div>
                        </div>

                        <div class="col-md-8 col-md-offset-4">
                        </div>
                        @else
                        <p class="alert alert-warning"><i class="fa fa-fw fa-warning"></i> Akun belum diaktifkan</p>
                        @endif
                    </div>

                    <div class="col-md-6">
                        <legend>MySQL Account</legend>

                        @if($permintaan->status == 'active')
                        <div class="form-group">
                            {!! Form::label('database[username]', 'Username', ['class' => 'control-label col-md-4']) !!}
                            <div class="col-md-8">
                                <p class="form-control-static">{!! $permintaan->username !!}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('database[password]', 'Password', ['class' => 'control-label col-md-4']) !!}
                            <div class="col-md-8">
                                <p class="form-control-static"><code>{!! $permintaan->password_db !!}</code></p>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('database[dbname]', 'Database Name', ['class' => 'control-label col-md-4']) !!}
                            <div class="col-md-8">
                                <p class="form-control-static">{!! $permintaan->db_name !!}</p>
                            </div>
                        </div>
                        @else
                        <p class="alert alert-warning"><i class="fa fa-fw fa-warning"></i> Akun belum diaktifkan</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endif
        
    </div>
</div>
@endsection