@extends('layout.backend')

@section('content')
<div class="row">
    <div class="col-md-12">
        <a href="{!! route('backend.user.create') !!}" class="btn btn-primary">Create</a>

        <hr>

        <div class="panel panel-default">
            <div class="panel-heading">
                Data
            </div>

            <div class="panel-body">
                <table class="table table-hover table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>NIM</th>
                            <th>Nama</th>
                            <th>No. HP</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($mahasiswa as $item)
                        <tr>
                            <td>{!! $item->id !!}</td>
                            <td>{!! $item->nim !!}</td>
                            <td>{!! $item->nama !!}</td>
                            <td>{!! $item->no_hp !!}</td>
                            <td>{!! $item->email !!}</td>
                            <td><a href="{!! route('backend.mahasiswa.edit', $item->id) !!}" class="btn btn-success btn-xs"><i class="fa fa-fw fa-pencil"></i></a></td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="5">Tidak ada data</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection