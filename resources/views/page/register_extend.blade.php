@extends('layout.portal')

@section('content')
<div class="row">
    <div class="col-md-offset-2 col-md-8">

        <div class="alert alert-danger">
            <strong>PENTING!</strong> Admin hanya akan mengaktifkan akun, jika data yang Anda kirimkan adalah benar.
        </div>

        {!! Form::model($mahasiswa, ['url' => url('register/extend')]) !!}

        <div class="row">
            <div class="col-md-6">
                <legend>Informasi Mahasiswa</legend>

                <div class="form-group">
                    {!! Form::label('nim', 'NIM') !!}
                    {!! Form::text('nim', null, ['class' => 'form-control', 'readonly' => true]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('nama', 'Nama Lengkap') !!}
                    {!! Form::text('nama', null, ['class' => 'form-control', 'readonly' => true]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('email') !!}
                    {!! Form::email('email', null, ['class' => 'form-control', 'readonly' => true]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('no_hp', 'No. HP') !!}
                    {!! Form::text('no_hp', null, ['class' => 'form-control', 'readonly' => true]) !!}
                </div>              
            </div>

            <div class="col-md-6">

                <legend>Informasi Skripsi</legend>

                <div class="form-group">
                    {!! Form::label('skripsi[judul]', 'Judul Skripsi') !!}
                    {!! Form::text('skripsi[judul]', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('skripsi[dosen_pembimbing]', 'Dosen Pembimbing') !!}
                    {!! Form::text('skripsi[dosen_pembimbing]', null, ['class' => 'form-control']) !!}
                </div>

                <legend>Informasi File</legend>                

                <div class="form-group">
                    {!! Form::label('permintaan[kapasitas]', 'Ukuran file akan diupload') !!}
                    <div class="input-group">
                        {!! Form::text('permintaan[kapasitas]', null, ['class' => 'form-control']) !!}
                        <span class="input-group-addon">
                            MB
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('permintaan[keterangan]', 'Keterangan') !!}
                    {!! Form::textarea('permintaan[keterangan]', null, ['class' => 'form-control', 'rows' => 3]) !!}
                </div>

            </div>
        </div>

        <legend>Submit</legend>

        <div class="form-group">
            {!! Form::submit('Register', ['class' => 'btn btn-primary']) !!}
        </div>

        {!! Form::close() !!}
    </div>
</div>
@endsection
