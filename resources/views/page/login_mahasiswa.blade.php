@extends('layout.portal')

@section('content')
<div class="row">
    <div class="col-md-offset-4 col-md-4">
        <legend>Akses Jurnal</legend>
            {!! Form::open(['route' => 'mahasiswa.login.submit']) !!}
                <fieldset>
                    <div class="form-group">
                        {!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'Username']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
                    </div>
                    
                    <!-- Change this to a button or input when using this as a form -->
                    {!! Form::submit('Login', ['class' => 'btn btn-lg btn-success btn-block']) !!}
                </fieldset>
            {!! Form::close() !!}
	</div>
</div>
@endsection