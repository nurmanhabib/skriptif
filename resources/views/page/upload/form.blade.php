@extends('layout.backend')

@section('content')

<p class="alert alert-info">Untuk upload jurnal, silahkan lewat akun FTP skripsi.tif.uad.ac.id dengan user <strong>jurnal</strong> / password <strong>skripsi</strong></p>


{!! Form::open(['route' => 'backend.upload.submit', 'files' => true]) !!}

    <div class="form-group">
        {!! Form::label('panduan', 'Panduan', ['class' => 'control-label']) !!}
        {!! Form::file('panduan') !!}
    </div>

    <div class="form-group">
        {!! Form::label('instruksi_kerja', 'Instruksi Kerja', ['class' => 'control-label']) !!}
        {!! Form::file('instruksi_kerja') !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Upload', ['class' => 'btn btn-primary']) !!}
    </div>

{!! Form::close() !!}

@endsection