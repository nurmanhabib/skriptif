@extends('layout.portal')

@section('content')
<div class="row">
    <div class="col-md-offset-2 col-md-8">

        <div class="alert alert-danger">
            <strong>PENTING!</strong> Admin hanya akan mengaktifkan akun, jika data yang Anda kirimkan adalah benar.
        </div>

        {!! Form::open(['url' => url('register/jurnal')]) !!}

        <div class="row">
            <div class="col-md-6">
                <legend>Informasi Mahasiswa</legend>

                <div class="form-group">
                    {!! Form::label('nim', 'NIM') !!}
                    {!! Form::text('nim', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('nama', 'Nama Lengkap') !!}
                    {!! Form::text('nama', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('email') !!}
                    {!! Form::email('email', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('no_hp', 'No. HP') !!}
                    {!! Form::text('no_hp', null, ['class' => 'form-control']) !!}
                </div>              
            </div>

            <div class="col-md-6">
                <legend>dan Login</legend>

                <div class="form-group">
                    {!! Form::label('password') !!}
                    {!! Form::password('password', ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('password_confirmation') !!}
                    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                </div>

            </div>
        </div>

        <legend>Submit</legend>

        <div class="form-group">
            {!! Form::submit('Register', ['class' => 'btn btn-primary']) !!}
        </div>

        {!! Form::close() !!}
    </div>
</div>
@endsection
