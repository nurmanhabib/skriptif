@extends('layout.backend')

@section('content')
<div class="panel panel-default">
    <div class="panel-body form-horizontal">
        <legend>Skripsi</legend>

        <div class="form-group">
            {!! Form::label('skripsi[judul]', 'Judul', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                <p class="form-control-static">{!! $skripsi->judul !!}</p>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('skripsi[dosen_pembimbing]', 'Dosen Pembimbing', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                <p class="form-control-static">{!! $skripsi->dosen_pembimbing !!}</p>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body form-horizontal">
        <legend>Mahasiswa</legend>

        <div class="form-group">
            {!! Form::label('mahasiswa[nim]', 'NIM', ['class' => 'control-label col-sm-2']) !!}
            <div class="col-sm-4">
                <p class="form-control-static">{!! $mahasiswa->nim !!}</p>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('mahasiswa[nama]', 'Nama', ['class' => 'control-label col-sm-2']) !!}
            <div class="col-sm-4">
                <p class="form-control-static">{!! $mahasiswa->nama !!}</p>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('mahasiswa[email]', 'Email', ['class' => 'control-label col-sm-2']) !!}
            <div class="col-sm-4">
                <p class="form-control-static">{!! $mahasiswa->email !!}</p>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('mahasiswa[no_hp]', 'No. HP', ['class' => 'control-label col-sm-2']) !!}
            <div class="col-sm-4">
                <p class="form-control-static">{!! $mahasiswa->no_hp !!}</p>
            </div>
        </div>
    </div>
</div>

{!! Form::open(['route' => ['backend.permintaan.destroy', $permintaan->id], 'method' => 'DELETE', 'style' => 'display: inline']) !!}
<div class="panel panel-default">
    <div class="panel-body form-horizontal">
        <div class="row">
            <div class="col-md-6">
                <legend>FTP Account</legend>

                <div class="form-group">
                    {!! Form::label('destroy_ftp', 'Hapus?', ['class' => 'control-label col-sm-4']) !!}
                    <div class="col-sm-8">
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox('destroy_ftp', 1, true) !!}
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <legend>MySQL Account</legend>

                <div class="form-group">
                    {!! Form::label('destroy_db', 'Hapus?', ['class' => 'control-label col-sm-4']) !!}
                    <div class="col-sm-8">
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox('destroy_db', 1, true) !!}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel-footer">
        <button class="btn btn-danger btn-delete"><i class="fa fa-fw fa-trash-o"></i> Konfirmasi Hapus</button> <a href="{!! route('backend.permintaan.show', $permintaan->id) !!}" class="btn btn-warning">Batal</a>
    </div>
</div>
{!! Form::close() !!}
@endsection