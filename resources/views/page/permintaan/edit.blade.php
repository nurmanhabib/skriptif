@extends('layout.backend')

@section('content')
<a href="{!! route('backend.permintaan.show', $permintaan->id) !!}" class="btn btn-default"><i class="fa fa-fw fa-chevron-left"></i> Kembali</a>

<hr>

{!! Form::model($permintaan, ['route' => ['backend.permintaan.update', $permintaan->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
<div class="panel panel-default">
    <div class="panel-body form-horizontal">
        <legend>Skripsi</legend>

        <div class="form-group">
            {!! Form::label('skripsi[judul]', 'Judul', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                {!! Form::textarea('skripsi[judul]', $skripsi->judul, ['class' => 'form-control', 'rows' => 3]) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('skripsi[dosen_pembimbing]', 'Dosen Pembimbing', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                {!! Form::text('skripsi[dosen_pembimbing]', $skripsi->dosen_pembimbing, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body form-horizontal">
        <legend>Mahasiswa</legend>

        <div class="form-group">
            {!! Form::label('mahasiswa[nim]', 'NIM', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-4">
                {!! Form::text('mahasiswa[nim]', $permintaan->mahasiswa->nim, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('mahasiswa[nama]', 'Nama', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-4">
                {!! Form::text('mahasiswa[nama]', $permintaan->mahasiswa->nama, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('mahasiswa[email]', 'Email', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-4">
                {!! Form::text('mahasiswa[email]', $permintaan->mahasiswa->email, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('mahasiswa[no_hp]', 'No. HP', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-4">
                {!! Form::text('mahasiswa[no_hp]', $permintaan->mahasiswa->no_hp, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    {!! Form::hidden('id', $permintaan->id) !!}
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <legend>FTP Account</legend>

                <div class="form-group">
                    {!! Form::label('ftp[username]', 'Username', ['class' => 'control-label col-md-4']) !!}
                    <div class="col-md-8">
                        {!! Form::text('ftp[username]', $permintaan->username, ['class' => 'form-control', 'readonly']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('ftp[password]', 'Password', ['class' => 'control-label col-md-4']) !!}
                    <div class="col-md-8">
                        {!! Form::text('ftp[password]', $permintaan->password_ftp, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('ftp[quota]', 'Limit Quota', ['class' => 'control-label col-md-4']) !!}
                    <div class="col-md-4">
                        <div class="input-group">
                            {!! Form::text('ftp[quota]', $permintaan->kapasitas, ['class' => 'form-control']) !!}
                            <span class="input-group-addon">MB</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <legend>MySQL Account</legend>

                <div class="form-group">
                    {!! Form::label('database[username]', 'Username', ['class' => 'control-label col-md-4']) !!}
                    <div class="col-md-8">
                        {!! Form::text('database[username]', $permintaan->username, ['class' => 'form-control', 'readonly']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('database[password]', 'Password', ['class' => 'control-label col-md-4']) !!}
                    <div class="col-md-8">
                        {!! Form::text('database[password]', $permintaan->password_db, ['class' => 'form-control', 'readonly']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('database[dbname]', 'Database Name', ['class' => 'control-label col-md-4']) !!}
                    <div class="col-md-8">
                        {!! Form::text('database[dbname]', $permintaan->db_name, ['class' => 'form-control', 'readonly']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-footer">
        <button class="btn btn-success"><i class="fa fa-fw fa-save"></i> Update</button>
    </div>
</div>
{!! Form::close() !!}
@endsection