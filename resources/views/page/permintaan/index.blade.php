@extends('layout.backend')

@section('style')
@parent
    <style>
    .skripsi-list-title {
        margin-top: 10px;
        font-weight: 600;
    }

    .skripsi-list-title small {
        font-weight: 100;
    }
    </style>
@endsection

@section('content')

<div class="row">
    <div class="col-md-6 col-md-offset-6">
        {!! Form::open(['route' => 'backend.permintaan.index', 'method' => 'GET']) !!}
        <div class="form-group">
            <div class="input-group">
                {!! Form::text('cari', null, ['class' => 'form-control', 'placeholder' => 'Pencarian']) !!}
                <div class="input-group-btn">
                    <button class="btn btn-primary"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}

<div class="row">
    <div class="col-md-12">
        <table class="table table-hover table-hover table-bordered">
            <thead>
                <tr>
                    <th width="5%">#</th>
                    <th>Judul Skripsi</th>
                    <th width="10%">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($permintaan as $item)
                <tr>
                    <td>{!! $item->id !!}</td>
                    <td><span title="{!! $item->created_at->toDateTimeString() !!}" class="label label-info">{!! $item->created_at->diffForHumans() !!}</span> <span class="label label-primary">{!! $item->mahasiswa->nama !!} ({!! $item->mahasiswa->nim !!})</span> <span class="label label-danger">{!! $item->kapasitas !!} MB</span> {!! $item->status_label !!}
                    <p class="skripsi-list-title">{!! $item->mahasiswa->skripsi->judul !!}<br>
                    <small>Dosen: {!! $item->mahasiswa->skripsi->dosen_pembimbing !!}</small></p>
                    </td>
                    <td>

                    @if($item->active)
                        <a href="{!! route('backend.permintaan.show', $item->id) !!}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-eye"></i></a>
                        <a href="{!! route('backend.permintaan.destroy.confirm', $item->id) !!}" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-trash-o"></i></a>
                    @elseif($item->suspend)
                        <a href="{!! route('backend.permintaan.unsuspend', $item->id) !!}" class="btn btn-success btn-xs"><i class="fa fa-fw fa-power-off"></i></a>
                    @else
                        <a href="{!! route('backend.permintaan.proccess', $item->id) !!}" class="btn btn-success btn-xs"><i class="fa fa-fw fa-check"></i></a>
                        {!! Form::open(['route' => ['backend.permintaan.destroy', $item->id], 'method' => 'DELETE']) !!}<button class="btn btn-danger btn-xs btn-delete"><i class="fa fa-fw fa-trash-o"></i></button>{!! Form::close() !!}
                    @endif

                    </td>
                </tr>
                @empty
                <tr class="warning">
                    <td colspan="6">Data tidak tersedia</td>
                </tr>
                @endforelse
            </tbody>
        </table>

        {!! $permintaan->render() !!}
    </div>
</div>
@endsection