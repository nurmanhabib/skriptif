@extends('layout.backend')

@section('content')
<div class="panel panel-default">
    <div class="panel-body form-horizontal">
        <legend>Skripsi</legend>

        <div class="form-group">
            {!! Form::label('skripsi[judul]', 'Judul', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                <p class="form-control-static">{!! $skripsi->judul !!}</p>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('skripsi[dosen_pembimbing]', 'Dosen Pembimbing', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                <p class="form-control-static">{!! $skripsi->dosen_pembimbing !!}</p>
            </div>
        </div>
    </div>

    <div class="panel-footer">
        <a href="{!! route('backend.permintaan.edit', $permintaan->id) !!}" class="btn btn-xs btn-warning"><i class="fa fa-fw fa-pencil"></i></a>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body form-horizontal">
        <legend>Mahasiswa</legend>

        <div class="form-group">
            {!! Form::label('mahasiswa[nim]', 'NIM', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-4">
                <p class="form-control-static">{!! $mahasiswa->nim !!}</p>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('mahasiswa[nama]', 'Nama', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-4">
                <p class="form-control-static">{!! $mahasiswa->nama !!}</p>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('mahasiswa[email]', 'Email', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-4">
                <p class="form-control-static">{!! $mahasiswa->email !!}</p>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('mahasiswa[no_hp]', 'No. HP', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-4">
                <p class="form-control-static">{!! $mahasiswa->no_hp !!}</p>
            </div>
        </div>
    </div>

    <div class="panel-footer">
        <a href="{!! route('backend.permintaan.edit', $permintaan->id) !!}" class="btn btn-xs btn-warning"><i class="fa fa-fw fa-pencil"></i></a>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body form-horizontal">
        <legend>Status</legend>

        <div class="form-group">
            {!! Form::label('permintaan[status]', 'Status', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                <p class="form-control-static">{!! $permintaan->status_label !!}</p>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('permintaan[activated_at]', 'Waktu Aktivasi', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                <p class="form-control-static">{!! $permintaan->activated_at !!}</p>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('permintaan[user_id]', 'Diaktivasi oleh', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                <p class="form-control-static">{!! $permintaan->user ? $permintaan->user->name : '' !!}</p>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('permintaan[expired_at]', 'Masa Berlaku', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-8">
                <p class="form-control-static">Aktif sampai <span class="label label-info">{{ $permintaan->expired_at }}</span></p>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body form-horizontal">
        <legend>Web</legend>

        <div class="form-group">
            {!! Form::label('homepage', 'Homepage', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                <p class="form-control-static">{!! link_to($skripsi->link, null, ['target' => '_blank']) !!}</p>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body form-horizontal">
        <div class="row">
            <div class="col-md-6">
                <legend>FTP Account</legend>

                @if($permintaan->status == 'active')
                <div class="form-group">
                    {!! Form::label('ftp[username]', 'Username', ['class' => 'control-label col-md-4']) !!}
                    <div class="col-md-8">
                        <p class="form-control-static">{!! $permintaan->username !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('ftp[password]', 'Password', ['class' => 'control-label col-md-4']) !!}
                    <div class="col-md-8">
                        <p class="form-control-static"><code>{!! $permintaan->password_ftp !!}</code></p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('ftp[quota]', 'Quota', ['class' => 'control-label col-md-4']) !!}
                    <div class="col-md-6">
                        <div class="form-control-static">
                            {!! $permintaan->getProgress() !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <p class="form-control-static">{!! $permintaan->kapasitas !!} MB</p>
                    </div>
                </div>
                @else
                <p class="alert alert-warning"><i class="fa fa-fw fa-warning"></i> Akun belum diaktifkan</p>
                @endif
            </div>

            <div class="col-md-6">
                <legend>MySQL Account</legend>

                @if($permintaan->status == 'active')
                <div class="form-group">
                    {!! Form::label('database[username]', 'Username', ['class' => 'control-label col-md-4']) !!}
                    <div class="col-md-8">
                        <p class="form-control-static">{!! $permintaan->username !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('database[password]', 'Password', ['class' => 'control-label col-md-4']) !!}
                    <div class="col-md-8">
                        <p class="form-control-static"><code>{!! $permintaan->password_db !!}</code></p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('database[dbname]', 'Database Name', ['class' => 'control-label col-md-4']) !!}
                    <div class="col-md-8">
                        <p class="form-control-static">{!! $permintaan->db_name !!}</p>
                    </div>
                </div>
                @else
                <p class="alert alert-warning"><i class="fa fa-fw fa-warning"></i> Akun belum diaktifkan</p>
                @endif
            </div>
        </div>
    </div>

    <div class="panel-footer">
        <a href="{!! route('backend.permintaan.edit', $permintaan->id) !!}" class="btn btn-xs btn-warning"><i class="fa fa-fw fa-pencil"></i></a>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        @if($permintaan->status == 'active')
            <a href="{!! route('backend.permintaan.active.sendmail', $permintaan->id) !!}" class="btn btn-primary"><i class="fa fa-fw fa-mail-forward"></i> Kirim Ulang Email</a> <a href="{!! route('backend.permintaan.destroy.confirm', $permintaan->id) !!}" class="btn btn-danger"><i class="fa fa-fw fa-trash-o"></i></a> <a href="{!! route('backend.permintaan.suspend', $permintaan->id) !!}" class="btn btn-warning"><i class="fa fa-fw fa-power-off"></i>Suspend</a>
        @elseif($permintaan->status == 'suspend')
            <a href="{!! route('backend.permintaan.destroy.confirm', $permintaan->id) !!}" class="btn btn-danger"><i class="fa fa-fw fa-trash-o"></i></a> <a href="{!! route('backend.permintaan.unsuspend', $permintaan->id) !!}" class="btn btn-success"><i class="fa fa-fw fa-power-off"></i>Unsuspend</a>
        @else
            <a href="{!! route('backend.permintaan.proccess', $permintaan->id) !!}" class="btn btn-success"><i class="fa fa-fw fa-check"></i> Aktivasi</a>
        @endif
    </div>
</div>
@endsection