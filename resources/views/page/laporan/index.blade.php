@extends('layout.backend')

@section('content')
{!! Form::open(['route' => 'backend.laporan.download', 'class' => 'form-horizontal']) !!}
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('daterange', 'Rentang Tanggal', ['class' => 'control-label col-md-4']) !!}
                    <div class="col-md-8">
                        {!! Form::text('daterange', null, ['class' => 'form-control daterange', 'placeholder' => 'Semua tanggal']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('angkatan', null, ['class' => 'control-label col-md-4']) !!}
                    <div class="col-md-8">
                        {!! Form::text('angkatan', null, ['class' => 'form-control', 'placeholder' => 'Semua angkatan. Format : YYYY']) !!}                        
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('status', null, ['class' => 'control-label col-md-4']) !!}
                    <div class="col-md-8">
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox('status[active]', true, true) !!} Active
                            </label>
                        </div> 
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox('status[pending]', true) !!} Pending
                            </label>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        {!! Form::submit('Generate', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
{!! Form::close() !!}
@endsection