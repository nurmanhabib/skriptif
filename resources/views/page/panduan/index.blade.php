@extends('layout.portal')

@section('script')
@parent
    {!! HTML::script('js/pdfobject.min.js') !!}

    <script type="text/javascript">

    window.onload = function (){

        var success = new PDFObject({ url: "upload/panduan.pdf" }).embed("pdf");
        
    };

    </script>
@endsection

@section('content')
    <div id="pdf">It appears you don't have Adobe Reader or PDF support in this web browser. <a href="upload/panduan.pdf">Click here to download the PDF</a></div>
@endsection