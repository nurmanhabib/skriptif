@extends('layout.portal')

@section('content')
@forelse($skripsi as $item)
<article class="post-entry">
    <header class="post-header">
        <h3 class="content-title">{!! link_to(route('mahasiswa.doc', $item->mahasiswa->nim), $item->judul) !!}</h3>
        <div class="post-meta">
            <small><span class="fa fa-user"></span> Admin </small>
            <small><span class="fa fa-calendar"></span> 16 01 2014</small>
            <small><span class="fa fa-list-alt"></span> <a href="">Category</a></small>
        </div>
   </header>

   <div class="post-content">
        <div class="col-md-3">
            <div class="post-image">
                <img src="img/thumbnail.jpg" class="img-thumbnail img-responsive" alt="">
            </div>
        </div>
     
        <div class="col-md-9">
            <div class="col-md-2">
                <ul>
                    <li>Penulis </li>
                    <li>Tahun </li>
                    <li>Pembimbing </li>
                    <li>Link</li>
                    @if (Auth::check())
                        <li>Skripsi PDF</li>
                    @endif
                </ul>
            </div>

            <div class="col-md-6">
                <ul>
                    <li> : {!! $item->mahasiswa->nama !!}</li>
                    <li> : {!! $item->tahun !!}</li>
                    <li> : {!! $item->dosen_pembimbing !!}</li>
                    <li> : {!! link_to($item->link, null, ['target' => '_blank']) !!}</li>
                    @if (Auth::check())
                        <li> : {!! link_to(route('mahasiswa.doc', $item->mahasiswa->nim), 'Download') !!}</li>
                    @endif
                </ul>
            </div>
        </div>      
    </div>
</article>
@empty
<p class="alert alert-warning">Tidak ada data yang ditampilkan.</p>
@endforelse

{!! $skripsi->render() !!}

@endsection
