@extends('layout.portal')

@section('content')

<table class="table table-hover table-bordered">
    <tbody>
        <tr>
            <td width="20%">Judul</td>
            <td>{{ strtoupper($mahasiswa->skripsi->judul) }}</td>
        </tr>
        <tr>
            <td width="20%">Penulis</td>
            <td>{{ $mahasiswa->full_name }}</td>
        </tr>
        <tr>
            <td>Tahun</td>
            <td>{{ $mahasiswa->skripsi->tahun }}</td>
        </tr>
        <tr>
            <td>Pembimbing</td>
            <td>{{ $mahasiswa->skripsi->dosen_pembimbing }}</td>
        </tr>
        <tr>
            <td>Link</td>
            <td><a href="{{ $mahasiswa->skripsi->link }}" target="_blank">{{ $mahasiswa->skripsi->link }}</a></td>
        </tr>
    </tbody>
</table>

<div class="file-dir">
    <h3>Files :</h3>

    @forelse($files as $file)
        <p><a href="{{ route('mahasiswa.doc.pdf', [$nim, $file['basename']]) }}"><i class="fa fa-fw fa-file-o"></i> {!! $file['filename'] !!}</a></p>
    @empty
        <p class="alert alert-danger">No Files Download</p>
    @endforelse
</div>
@endsection