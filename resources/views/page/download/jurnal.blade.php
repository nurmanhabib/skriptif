@extends('layout.portal')

@section('content')
<div class="file-dir">
    <h3>Folder :</h3>

    @foreach($dirs as $dir)
        <p><a href="{{ route('jurnal.show', $dir['path']) }}"><i class="fa fa-fw fa-folder-o"></i> {!! $dir['filename'] !!}</a></p>
    @endforeach

    {!! $dirs->render() !!}

    <h3>Files :</h3>

    @forelse($files as $file)
        <p><a href="{{ route('jurnal.download') . '?filename=' . $file['path'] }}"><i class="fa fa-fw fa-file-o"></i> {!! $file['filename'] !!}</a></p>
    @empty
        <p class="alert alert-danger">No Files Download</p>
    @endforelse
</div>
@endsection
