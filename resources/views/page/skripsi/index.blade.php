@extends('layout.backend')

@section('content')
<div class="row">
    <div class="col-md-12">
        <a href="{!! route('backend.skripsi.create') !!}" class="btn btn-primary">Create</a>

        <hr>

        <div class="panel panel-default">
            <div class="panel-heading">
                Skripsi
            </div>

            <div class="panel-body">
                <table class="table table-hover table-hover table-bordered">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="20%">Nama</th>
                            <th width="45%">Judul Skripsi</th>
                            <th width="20%">Dosen Pembimbing</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($skripsi as $item)
                        <tr>
                            <td>{!! $item->id !!}</td>
                            <td>{!! $item->mahasiswa->nama !!}<br><small>{!! $item->mahasiswa->nim !!}</small></td>
                            <td>{!! $item->judul !!}</td>
                            <td>{!! $item->dosen_pembimbing !!}</td>
                            <td><a href="{!! route('backend.skripsi.edit', $item->id) !!}" class="btn btn-success btn-xs"><i class="fa fa-fw fa-pencil"></i></a> {!! Form::open(['route' => ['backend.user.destroy', $item->id], 'method' => 'DELETE']) !!}<button class="btn btn-danger btn-xs btn-delete"><i class="fa fa-fw fa-trash-o"></i></button>{!! Form::close() !!}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6">Tidak ada data</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>

                {!! $skripsi->render() !!}
            </div>
        </div>
    </div>
</div>
@endsection