@extends('layout.backend')

@section('style')
@parent
    <style>
    table a.btn {
        float: left;
        margin-right: 5px;
    }
    </style>
@endsection

@section('content')
<a href="{!! route('backend.user.create') !!}" class="btn btn-primary">Create</a>

<hr>

<table class="table table-hover table-hover table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th>Nama</th>
            <th>Username</th>
            <th>Email</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse($users as $user)
        <tr>
            <td>{!! $user->id !!}</td>
            <td>{!! $user->profile ? $user->profile->full_name : '' !!}</td>
            <td>{!! $user->name !!}</td>
            <td>{!! $user->email !!}</td>
            <td><a href="{!! route('backend.user.edit', $user->id) !!}" class="btn btn-success btn-xs"><i class="fa fa-fw fa-pencil"></i></a> {!! Form::open(['route' => ['backend.user.destroy', $user->id], 'method' => 'DELETE']) !!}<button href="{!! url() !!}" class="btn btn-danger btn-xs btn-delete"><i class="fa fa-fw fa-trash-o"></i></button>{!! Form::close() !!}</td>
        </tr>
        @empty
        <tr>
            <td colspan="5">Tidak ada data</td>
        </tr>
        @endforelse
    </tbody>
</table>
@endsection