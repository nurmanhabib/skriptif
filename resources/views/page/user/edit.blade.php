@extends('layout.backend')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Informasi
            </div>

            {!! Form::model($user, ['route' => ['backend.user.update', $user->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('name', 'Username', ['class' => 'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('password', null, ['class' => 'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                <a href="{!! route('backend.user.password', $user->id) !!}" class="btn btn-primary">Change password</a>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('email', null, ['class' => 'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                {!! Form::text('email', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('status', null, ['class' => 'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                {!! Form::select('status', ['Nonaktif', 'Aktif'], null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('profile[first_name]', 'Nama Depan', ['class' => 'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                {!! Form::text('profile[first_name]', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('profile[last_name]', 'Nama Belakang', ['class' => 'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                {!! Form::text('profile[last_name]', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('profile[address]', 'Address', ['class' => 'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                {!! Form::text('profile[address]', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('profile[phone_number]', 'Phone Number', ['class' => 'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                {!! Form::text('profile[phone_number]', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
    
                        <div class="form-group">
                            {!! Form::label('profile[place_of_birth]', 'Tempat Lahir', ['class' => 'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                {!! Form::text('profile[place_of_birth]', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('profile[date_of_birth]', 'Tanggal Lahir', ['class' => 'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                {!! Form::text('profile[date_of_birth]', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-footer">
                <button class="btn btn-primary">Update</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection