@extends('layout.backend')

@section('content')
<div class="panel panel-default">
    {!! Form::open(['route' => 'backend.user.store', 'class' => 'form-horizontal']) !!}
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <legend>Informasi Login</legend>

                <div class="form-group">
                    {!! Form::label('name', 'Username', ['class' => 'control-label col-sm-4']) !!}
                    <div class="col-sm-8">
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('password', null, ['class' => 'control-label col-sm-4']) !!}
                    <div class="col-sm-8">
                        {!! Form::password('password', ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('password_confirmation', null, ['class' => 'control-label col-sm-4']) !!}
                    <div class="col-sm-8">
                        {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('email', null, ['class' => 'control-label col-sm-4']) !!}
                    <div class="col-sm-8">
                        {!! Form::text('email', null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <legend>Hak Akses</legend>

                <div class="form-group">
                    {!! Form::label('status', null, ['class' => 'control-label col-sm-4']) !!}
                    <div class="col-sm-8">
                        {!! Form::select('status', ['Nonaktif', 'Aktif'], 1, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('level', 'Super Admin', ['class' => 'control-label col-sm-4']) !!}
                    <div class="col-sm-8">
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox('level', 1) !!}
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <legend>Profile</legend>

                <div class="form-group">
                    {!! Form::label('profile[first_name]', 'Nama Depan', ['class' => 'control-label col-sm-4']) !!}
                    <div class="col-sm-8">
                        {!! Form::text('profile[first_name]', null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('profile[last_name]', 'Nama Belakang', ['class' => 'control-label col-sm-4']) !!}
                    <div class="col-sm-8">
                        {!! Form::text('profile[last_name]', null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('profile[address]', 'Address', ['class' => 'control-label col-sm-4']) !!}
                    <div class="col-sm-8">
                        {!! Form::text('profile[address]', null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('profile[phone_number]', 'Phone Number', ['class' => 'control-label col-sm-4']) !!}
                    <div class="col-sm-8">
                        {!! Form::text('profile[phone_number]', null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('profile[place_of_birth]', 'Tempat Lahir', ['class' => 'control-label col-sm-4']) !!}
                    <div class="col-sm-8">
                        {!! Form::text('profile[place_of_birth]', null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('profile[date_of_birth]', 'Tanggal Lahir', ['class' => 'control-label col-sm-4']) !!}
                    <div class="col-sm-8">
                        {!! Form::text('profile[date_of_birth]', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel-footer">
        <button class="btn btn-primary">Create</button>
    </div>
    {!! Form::close() !!}
</div>
@endsection