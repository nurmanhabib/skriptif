@extends('layout.portal')

@section('content')
<div class="row">
    <div class="col-md-12">
        <p class="alert alert-success">
            <strong>PENTING!</strong> Silahkan melakukan aktivasi ke Laboran Lab Jaringan (Pak Sigit) dengan membawa Kartu Bimbingan Skripsi. Anda akan mendapatkan informasi login melalui email setelah Admin menyetujui.
        </p>

        <a href="{!! route('home') !!}" class="btn btn-primary">Kembali ke home</a>
    </div>
</div>
@endsection
