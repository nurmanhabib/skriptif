<html>
<head>
    <meta charset="UTF-8">
    <title>Portal Skripsi Teknik Informatika</title>

    <!-- Untuk CSS -->
    @section('style')
    {!! HTML::style('css/bootstrap.css') !!}
    {!! HTML::style('css/stylesheet.css') !!}
    {!! HTML::style('css/font-awesome.css') !!}
    {!! HTML::style('css/font.css') !!}
    {!! HTML::style('css/style.css') !!}
    {!! HTML::style('http://fonts.googleapis.com/css?family=Lato:400,700') !!}
    @show

</head>

<body id="offcanvas-container" class="offcanvas-container fs12 page-home " data-twttr-rendered="true">

    <!--header-->
    <section id="header">

        <!--topbar-->
        <section id="topbar">
        </section>
        <!--end top bar-->


        <!--Header Main-->
        <section id="header-main">
            <div class="container">
                <div class="row header-wrap">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 inner">
                        <div class="logo">
                            <a href="{!! url('/') !!}">
                                <img src="{!! asset('img/logo.png') !!}" class="img-responsive">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Header-->

        <!--Navbar-->
        <section id="pav-mainnav">
            <div class="container">
                <nav class="navbar navbar-default" role="navigation">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li>{!! link_to('/', 'Home') !!}</li>
                            <li>{!! link_to('download', 'Download') !!}</li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Register <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li>{!! link_to('register/jurnal', 'Register Jurnal') !!}</li>
                                    <li>{!! link_to('register', 'Register Hosting') !!}</li>
                                </ul>
                            </li>
                            <li>{!! link_to('panduan', 'Panduan') !!}</li>
                            <li>{!! link_to('instruksi-kerja', 'Instruksi Kerja (IK)') !!}</li>
                        </ul>
                     
                        <ul class="nav navbar-nav navbar-right">
                            @if(Auth::guest())
                            <li>{!! link_to(route('mahasiswa.login'), 'Login') !!}</li>
                            @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Akun <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li>{!! link_to(route('mahasiswa.edit'), 'Profile [' . Auth::user()->name . ']') !!}</li>
                                    <li>{!! link_to(route('mahasiswa.logout'), 'Logout') !!}</li>
                                </ul>
                            </li>
                            @endif
                            <form action="{!! route('home') !!}" class="navbar-form navbar-left" role="search">
                                <div class="form-group">
                                    {!! Form::text('cari', Input::get('cari'), ['class' => 'form-control', 'placeholder' => 'Search']) !!}
                                </div>
                                <button type="submit" class="btn btn-default fa fa-search"></button>
                            </form>
                        </ul>
                    </div> <!-- /.navbar-collapse -->
                </nav>
            </div>
        </section> <!--End Navbar-->
    </section> <!--End Header-->
    
    <section id="columns" class="offcanvas-siderbars">
        <div class="container">
            <div class="row">
            
                <section class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-column">         
                    <div id="content">

                        <!--Slider-->   
                        <div id="carousel-example-captions" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img alt="900x500" src="{!! asset('img/url.png') !!}">          
                                </div>
                                <div class="item">
                                    <img alt="900x500" src="{!! asset('img/slide1.png') !!}">          
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-captions" data-slide="prev"></a>
                            <a class="right carousel-control" href="#carousel-example-captions" data-slide="next"></a>
                        </div>
                        <!--End Slider-->

                        <!--Artikel-->
                        <div class="container">
                            <div class="page-header">
                                <h1><i class="fa fa-desktop fa-fw"></i> {!! $page_title or "Default Page" !!}</h1>
                            </div>
                            <div class="col-md-12">

                                @include('errors.session')

                                @section('content')
                                    <p>No content here </p>
                                @show

                            </div>
                        </div>
                        <!-- End Artikel -->
                    </div>
                    <!--End Content-->
                </section>
            </div>
        </div>
    </section>

    <!--Footer-->
    <section id="footer">
        <div class="footer-center">
            <div class="container">
                <div class="row">
                    <div class="column col-xs-12 col-sm-6 col-lg-3">
                        <div class="box contact-us">
                            <div class="box-heading">
                                <i class="fa fa-map-marker"></i><span><span> Contact</span> Us</span>
                            </div>
                            <p>
                                Kampus 3<br>
                                Jl. Prof. Dr. Soepomo, Janturan, Umbulharjo, Yogyakarta 55164
                            </p>
                        </div>
                    </div>
                                      
                    <div class="column col-xs-12 col-sm-6 col-lg-3">
                        <div class="box">
                            <div class="box-heading"><i class="fa fa-folder-open"></i><span> Kategori</span></div>
                            <ul class="list">
                                <li><a href="">Uncategory</a></li>
                                <li><a href="">Kampus</a></li>
                                <li><a href="">Berita</a></li>
                                <li><a href="">Pengumuman</a></li>
                            </ul>
                        </div>
                    </div>
                      
                    <div class="column col-xs-12 col-sm-6 col-lg-3">
                        <div class="box">
                        <div class="box-heading"><i class="fa fa-check"></i><span> Follow Us</span></div>
                        <div class="social">
                            <ul class="list">
                                <li><a href="#"><i class="fa fa-facebook"></i>Facebook</a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i>Twitter</a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i>GooglePlus</a> </li>
                            </ul>
                        </div>
                      </div>
                    </div>
                      
                    <div class="column col-xs-12 col-sm-6 col-lg-3">
                        <div class="box">
                            <div class="box-heading"><i class="fa fa-link"></i><span> Link Terkait</span></div>
                            <ul class="list">
                                <li>{!! link_to('http://tif.uad.ac.id', 'Teknik Informatika UAD') !!}</li>
                                <li>{!! link_to('http://fti.uad.ac.id', 'FTI UAD') !!}</li>
                                <li>{!! link_to('http://uad.ac.id', 'UAD') !!}</li>
                            </ul>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        
        <div id="powered">
            <div class="container">
                <div class="copyright pull-left"> 
                    Copyright &copy 2015 {!! link_to('/', 'Portal Skripsi') !!}
                </div>
                <div class="copyright pull-right"> 
                    <a href="http://www.tif.uad.ac.id">Teknik Informatika</a>
                </div>
            </div>
        </div>
    </section>

    <!--Script-->
    @section('script')
    {!! HTML::script('js/jquery-1.9.1.min.js') !!}
    {!! HTML::script('js/bootstrap.js') !!}
    {!! HTML::script('js/jquery.min.js') !!}
    @show
    
</body>
</html>