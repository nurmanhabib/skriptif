<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{!! $page_title or "Dashboard" !!} - {!! $site_title or "Control Panel Skripsi TIF UAD" !!}</title>

    @section('style')
    <!-- Bootstrap Core CSS -->
    {!! HTML::style('bower_components/bootstrap/dist/css/bootstrap.min.css') !!}

    <!-- MetisMenu CSS -->
    {!! HTML::style('bower_components/metisMenu/dist/metisMenu.min.css') !!}

    <!-- Custom CSS -->
    {!! HTML::style('dist/css/sb-admin-2.css') !!}

    <!-- Custom Fonts -->
    {!! HTML::style('bower_components/font-awesome/css/font-awesome.min.css') !!}

    {!! HTML::style('css/daterangepicker-bs3.css') !!}

    {!! HTML::style('css/skriptif.css') !!}
    @show

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{!! route('backend') !!}">{!! $site_title or "Control Panel Skripsi TIF UAD" !!}</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="{!! route('backend.user.edit', $my->id) !!}"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{!! route('logout') !!}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">

                    {!! Navigator::show() !!}

                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{!! $page_title or "Dashboard" !!}</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->

                <div class="row">
                    @include('errors.session')
                </div>

                @section('content')
                    <p>No content here.</p>
                @show
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    @section('script')
    <!-- jQuery -->
    {!! HTML::script('bower_components/jquery/dist/jquery.min.js') !!}

    <!-- Bootstrap Core JavaScript -->
    {!! HTML::script('bower_components/bootstrap/dist/js/bootstrap.min.js') !!}

    <!-- Metis Menu Plugin JavaScript -->
    {!! HTML::script('bower_components/metisMenu/dist/metisMenu.min.js') !!}

    <!-- Custom Theme JavaScript -->
    {!! HTML::script('dist/js/sb-admin-2.js') !!}
    
    {!! HTML::script('js/moment.min.js') !!}
    {!! HTML::script('js/daterangepicker.js') !!}

    <!-- Custom Web JavaScript -->
    {!! HTML::script('js/skriptif.js') !!}
    @show

</body>

</html>
