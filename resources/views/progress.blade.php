<div class="progress">
    <div class="progress-bar" role="progressbar" aria-valuenow="{{ $permintaan->current_percent }}" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: {{ $permintaan->current_percent }}%;">
        {{ $permintaan->current }} MB
    </div>
</div>