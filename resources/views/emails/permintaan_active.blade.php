@extends('layout.email')

@section('content')
  <table class="row">
    <tr>
      <td class="wrapper last">

        <table class="twelve columns">
          <tr>
            <td>
              <h1>Hi, {!! $mahasiswa->nama !!}</h1>
  						<p class="lead">Kami telah mengaktifkan akun Anda. Berikut detail akun FTP dan MySQL.</p>

              <table>
                <tr>
                  <td width="25%">NIM</td>
                  <td width="5%"> : </td>
                  <td>{!! $mahasiswa->nim !!}</td>
                </tr>
                <tr>
                  <td width="25%">Nama</td>
                  <td width="5%"> : </td>
                  <td>{!! $mahasiswa->nama !!}</td>
                </tr>
                <tr>
                  <td width="25%">Judul Skripsi</td>
                  <td width="5%"> : </td>
                  <td>{!! $mahasiswa->skripsi->judul !!}</td>
                </tr>
                <tr>
                  <td>Dosen Pembimbing</td>
                  <td> : </td>
                  <td>{!! $mahasiswa->skripsi->dosen_pembimbing !!}</td>
                </tr>
              </table>

              <h3>Homepage URL</h3>

              <table>
                <tr>
                  <td width="25%">Your Homepage</td>
                  <td width="5%"> : </td>
                  <td>{!! $mahasiswa->skripsi->link !!}</td>
                </tr>
              </table>

              <h3>FTP Account</h3>

              <table>
                <tr>
                  <td width="25%">Host</td>
                  <td width="5%"> : </td>
                  <td>skripsi.tif.uad.ac.id</td>
                </tr>
                <tr>
                  <td width="25%">Port</td>
                  <td width="5%"> : </td>
                  <td>21</td>
                </tr>
                <tr>
                  <td width="25%">Username</td>
                  <td width="5%"> : </td>
                  <td>{!! $permintaan->username !!}</td>
                </tr>
                <tr>
                  <td width="25%">Password</td>
                  <td width="5%"> : </td>
                  <td>{!! $permintaan->password_ftp !!}</td>
                </tr>
              </table>

              <h3>MySQL Account</h3>

              <table>
                <tr>
                  <td width="25%">PHPMyAdmin</td>
                  <td width="5%"> : </td>
                  <td>{!! url('phpmyadmin') !!}</td>
                </tr>
                <tr>
                  <td width="25%">Username</td>
                  <td width="5%"> : </td>
                  <td>{!! $permintaan->username !!}</td>
                </tr>
                <tr>
                  <td width="25%">Password</td>
                  <td width="5%"> : </td>
                  <td>{!! $permintaan->password_db !!}</td>
                </tr>
              </table>

  						<p>Anda melakukan registrasi dengan besar file <strong>{!! $mahasiswa->permintaan->kapasitas !!} MB</strong>.</p>
            </td>
            <td class="expander"></td>
          </tr>
        </table>

      </td>
    </tr>
  </table>

  <table class="row callout">
    <tr>
      <td class="wrapper last">

        <table class="twelve columns">
          <tr>
            <td class="panel">
              <p><strong>Status: Aktif</strong></p>
              <p>Akun FTP dan MySQL HANYA dapat diakses melalui jaringan lokal kampus UAD. Tidak dapat diakses dari luar kampus UAD.</p>
            </td>
            <td class="expander"></td>
          </tr>
        </table>

      </td>
    </tr>
  </table>
@endsection