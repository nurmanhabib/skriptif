@extends('layout.email')

@section('content')
  <table class="row">
    <tr>
      <td class="wrapper last">

        <table class="twelve columns">
          <tr>
            <td>
              <h1>Hi, {!! $mahasiswa->nama !!}</h1>
  						<p class="lead">Proses registrasi skripsi telah berhasil, namun belum aktif.</p>

              <table>
                <tr>
                  <td width="25%">NIM</td>
                  <td width="5%"> : </td>
                  <td>{!! $mahasiswa->nim !!}</td>
                </tr>
                <tr>
                  <td width="25%">Nama</td>
                  <td width="5%"> : </td>
                  <td>{!! $mahasiswa->nama !!}</td>
                </tr>
                <tr>
                  <td width="25%">Judul Skripsi</td>
                  <td width="5%"> : </td>
                  <td>{!! $mahasiswa->skripsi->judul !!}</td>
                </tr>
                <tr>
                  <td>Dosen Pembimbing</td>
                  <td> : </td>
                  <td>{!! $mahasiswa->skripsi->dosen_pembimbing !!}</td>
                </tr>
              </table>

  						<p>Anda melakukan registrasi dengan besar file <strong>{!! $mahasiswa->permintaan->kapasitas !!} MB</strong>.</p>
            </td>
            <td class="expander"></td>
          </tr>
        </table>

      </td>
    </tr>
  </table>

  <table class="row callout">
    <tr>
      <td class="wrapper last">

        <table class="twelve columns">
          <tr>
            <td class="panel">
              <p><strong>Status: Pending</strong></p>
              <p>Mohon tunggu sampai Anda mendapatkan email bahwa akun FTP dan MySQL sudah aktif.</p>
            </td>
            <td class="expander"></td>
          </tr>
        </table>

      </td>
    </tr>
  </table>
@endsection