@extends('layout.email')

@section('content')
  <table class="row">
    <tr>
      <td class="wrapper last">

        <table class="twelve columns">
          <tr>
            <td>
              <h1>Hi, {!! $mahasiswa->nama !!}</h1>
  						<p class="lead">Proses registrasi akses jurnal telah berhasil, namun belum aktif.</p>

              <table>
                <tr>
                  <td width="25%">NIM</td>
                  <td width="5%"> : </td>
                  <td>{!! $mahasiswa->nim !!}</td>
                </tr>
                <tr>
                  <td width="25%">Nama</td>
                  <td width="5%"> : </td>
                  <td>{!! $mahasiswa->nama !!}</td>
                </tr>
              </table>

            </td>
            <td class="expander"></td>
          </tr>
        </table>

      </td>
    </tr>
  </table>

  <table class="row callout">
    <tr>
      <td class="wrapper last">

        <table class="twelve columns">
          <tr>
            <td class="panel">
              <p><strong>Status: Pending</strong></p>
              <p>Mohon tunggu sampai Anda mendapatkan email bahwa akun sudah aktif.</p>
            </td>
            <td class="expander"></td>
          </tr>
        </table>

      </td>
    </tr>
  </table>
@endsection