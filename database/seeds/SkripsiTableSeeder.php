<?php

use Illuminate\Database\Seeder;

use App\Mahasiswa;
use App\Skripsi;

class SkripsiTableSeeder extends Seeder {
    
    public function run()
    {
        DB::table('skripsi')->delete();

        Skripsi::create([
            'judul'             => 'Simulasi Penerapan OWASP (The Open Web Application Security Project) Untuk Pembelajaran Keamanan Web (Studi Kasus : CMS Wordpress)',
            'dosen_pembimbing'  => 'Imam Riadi',
            'mahasiswa_id'      => Mahasiswa::nim('10018100')->id,
        ]);

        Skripsi::create([
            'judul'             => 'Analisis Perancangan Firewall Paket Filtering dan Proxy Server Untuk Optimasi Bandwidth (Studi Kasus Di Lab. Riset Kampus 3 Universitas Ahmad Dahlan)',
            'dosen_pembimbing'  => 'Imam Riadi',
            'mahasiswa_id'      => Mahasiswa::nim('10018145')->id,
        ]);

        Skripsi::create([
            'judul'             => 'Implementasi CRM (Customer Relationship Management) Untuk Layanan Tamu Di Kecamatan Berbah Sleman Yogyakarta',
            'dosen_pembimbing'  => 'Imam Riadi',
            'mahasiswa_id'      => Mahasiswa::nim('08018298')->id,
        ]);

        Skripsi::create([
            'judul'             => 'Implementasi Proxy Server dan Load Balancing Menggunakan Metode Per Connection Classfier Berbasis Mikrotik',
            'dosen_pembimbing'  => 'Imam Riadi',
            'mahasiswa_id'      => Mahasiswa::nim('08018093')->id,
        ]);

        Skripsi::create([
            'judul'             => 'Desain dan Implementasi Virtualisasi Server Menggunakan Proxmox',
            'dosen_pembimbing'  => 'Imam Riadi',
            'mahasiswa_id'      => Mahasiswa::nim('07018096')->id,
        ]);
    }

}