<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Profile;

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();
        DB::table('profiles')->delete();

        $user = User::create([
            'name'      => 'skripsi',
            'email'     => 'skripsi@tif.uad.ac.id',
            'password'  => 'password',
            'status'    => 1,
            'level'     => 0,
        ]);

        Profile::create([
            'first_name'    => 'Admin',
            'last_name'     => 'Skriptif',
            'address'       => 'Yogyakarta',
            'phone_number'  => '0274 555444',
            'user_id'       => $user->id,
        ]);
    }

}