<?php

use Illuminate\Database\Seeder;

use App\Mahasiswa;

class MahasiswaTableSeeder extends Seeder {

    public function run()
    {
        DB::table('mahasiswa')->delete();

        Mahasiswa::create([
            'nim'   => '10018100',
            'nama'  => 'Candra Dwi Waskito',
            'email' => 'candra@skripsi.tif.uad.ac.id',
            'no_hp' => '08123456789',
        ]);

        Mahasiswa::create([
            'nim'   => '10018145',
            'nama'  => 'Yanuar Dwi Jatmiko Wismoaji',
            'email' => 'yanuar@skripsi.tif.uad.ac.id',
            'no_hp' => '08123456789',
        ]);

        Mahasiswa::create([
            'nim'   => '08018298',
            'nama'  => 'Galih Pamungkas',
            'email' => 'galih@skripsi.tif.uad.ac.id',
            'no_hp' => '08123456789',
        ]);

        Mahasiswa::create([
            'nim'   => '08018093',
            'nama'  => 'Fiki Justisia Bahayangkara',
            'email' => 'fiki@skripsi.tif.uad.ac.id',
            'no_hp' => '08123456789',
        ]);

        Mahasiswa::create([
            'nim'   => '07018096',
            'nama'  => 'Dicky Martin Pramanta',
            'email' => 'dicky@skripsi.tif.uad.ac.id',
            'no_hp' => '08123456789',
        ]);
    }

}