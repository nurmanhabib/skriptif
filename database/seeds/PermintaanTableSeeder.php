<?php

use Illuminate\Database\Seeder;

use App\Mahasiswa;
use App\Permintaan;

class PermintaanTableSeeder extends Seeder {

	public function run()
	{
		DB::table('permintaan')->delete();

		Permintaan::create([
			'kapasitas'		=> 50,
			'keterangan'	=> '',
			'status'		=> 'pending',
			'mahasiswa_id'	=> Mahasiswa::nim('10018100')->id,
		]);

		Permintaan::create([
			'kapasitas'		=> 40,
			'keterangan'	=> '',
			'status'		=> 'pending',
			'mahasiswa_id'	=> Mahasiswa::nim('10018145')->id,
		]);

		Permintaan::create([
			'kapasitas'		=> 50,
			'keterangan'	=> '',
			'status'		=> 'pending',
			'mahasiswa_id'	=> Mahasiswa::nim('08018298')->id,
		]);

		Permintaan::create([
			'kapasitas'		=> 25,
			'keterangan'	=> '',
			'status'		=> 'pending',
			'mahasiswa_id'	=> Mahasiswa::nim('08018093')->id,
		]);

		Permintaan::create([
			'kapasitas'		=> 40,
			'keterangan'	=> '',
			'status'		=> 'pending',
			'mahasiswa_id'	=> Mahasiswa::nim('07018096')->id,
		]);
	}

}