<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkripsiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('skripsi', function(Blueprint $t)
		{
			$t->increments('id');
			$t->string('judul');
			$t->string('tahun', 4);
			$t->string('dosen_pembimbing');
			$t->integer('mahasiswa_id');
			$t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('skripsi');
	}

}
