<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPasswordFieldPermintaan extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('permintaan', function(Blueprint $t)
		{
			$t->string('password_ftp', 8)->after('keterangan');
			$t->string('password_db', 20)->after('password_ftp');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('permintaan', function(Blueprint $t)
		{
			$t->dropColumn('password_ftp');
			$t->dropColumn('password_db');
		});
	}

}
