<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteTahunSkripsi extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('skripsi', function(Blueprint $t)
		{
			$t->dropColumn('tahun');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('skripsi', function(Blueprint $t)
		{
			$t->string('tahun', 4);
		});
	}

}
