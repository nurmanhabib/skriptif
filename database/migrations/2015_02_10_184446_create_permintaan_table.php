<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermintaanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('permintaan', function(Blueprint $t)
		{
			$t->increments('id');
			$t->integer('kapasitas');
			$t->integer('mahasiswa_id');
			$t->enum('status', ['pending', 'active', 'suspend'])->default('pending');
			$t->integer('user_id');
			$t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('permintaan');
	}

}
