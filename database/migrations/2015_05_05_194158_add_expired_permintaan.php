<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpiredPermintaan extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('permintaan', function(Blueprint $table)
		{
			$table->dateTime('expired_at')->after('activated_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('permintaan', function(Blueprint $table)
		{
			$table->dropColumn('expired_at');
		});
	}

}
