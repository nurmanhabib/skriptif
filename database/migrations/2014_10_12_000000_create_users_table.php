<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->unique();
			$table->string('email')->unique();
			$table->string('password', 60);
			$table->tinyInteger('status')->default(0);
			$table->tinyInteger('level')->default(1);
			$table->rememberToken();
			$table->timestamps();
		});

        Schema::create('profiles', function(Blueprint $t)
        {
            $t->increments('id');
            $t->string('first_name');
            $t->string('last_name');
            $t->string('address');
            $t->string('phone_number');
            $t->string('place_of_birth');
            $t->date('date_of_birth');
            $t->integer('user_id')->unique();
            $t->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
		Schema::drop('profiles');
	}

}
