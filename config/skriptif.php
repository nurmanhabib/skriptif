<?php

return [

    'dir' => [

        'mahasiswa' => env('DIR_MAHASISWA', storage_path() . '/app/doc'),
        'jurnal'    => env('DIR_JURNAL', storage_path() . '/app/jurnal'),

    ]

];